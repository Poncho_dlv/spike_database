#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class BB2Resources:

    def __init__(self):
        self.__dbFileName = "databases/Rules.db"

    def get_race_id(self, race_label):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        if race_label == "Lizardmen":
            race_label = "Lizardman"
        cursor.execute("""SELECT ID FROM Races WHERE DataConstant=?""", (race_label,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return response
        else:
            return response[0]

    def get_race_label(self, race_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT DataConstant FROM Races WHERE ID=?""", (race_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return response
        else:
            return response[0]

    def get_lineman_value(self, race_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT Price FROM PlayerTypes WHERE IdRaces=? AND MaxQuantity=16""", (race_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return response
        else:
            return response[0]

    def get_race_id_with_short_name(self, short_name):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT ID FROM Races WHERE DataConstant LIKE ? """, ("{}%".format(short_name),))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        else:
            return response[0]

    def get_player_id(self, player_type):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT ID FROM PlayerTypes WHERE DataConstant=?""", (player_type,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return response
        else:
            return response[0]

    def get_skills_id(self, player_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT IdSkillListing FROM PlayerTypeSkills WHERE IdPlayerTypes=?""", (player_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def get_skills_name(self, skill_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT DataConstant FROM SkillListing WHERE ID=?""", (skill_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return response
        else:
            return response[0]
