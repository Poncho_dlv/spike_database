#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum

from spike_database.BaseMongoDb import BaseMongoDb


class StatisticType(IntEnum):
    OVERALL = 0
    LEAGUE = 1
    COMPETITION = 2
    RANKED_LADDER = 3
    RANKED_PLAYOFF = 4


class CoachesStatistics(BaseMongoDb):

    def save_coach_statistic(self, coach_id: int, platform_id: int, statistics: list):
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id}
        collection.delete_many(query)
        collection.insert_many(statistics)
        self.close_database()

    def coach_stat_exist(self, coach_id: int, platform_id: int, version: int = 3):
        stat = self.get_coach_statistic(coach_id, platform_id, version=version)
        return stat is not None

    def get_competitions_played(self, coach_id: int, platform_id: int, version: int = 3):
        competition_list = []
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id, "type": StatisticType.COMPETITION, "version": version}
        output = {"_id": 0, "name": 1}
        ret = collection.find(query, output).distinct("name")
        for cmp in ret:
            competition_list.append(cmp)
        self.close_database()
        return competition_list

    def get_leagues_played(self, coach_id: int, platform_id: int, version: int = 3):
        competition_list = []
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id, "type": StatisticType.LEAGUE, "version": version}
        output = {"_id": 0, "name": 1}
        ret = collection.find(query, output).distinct("name")
        for cmp in ret:
            competition_list.append(cmp)
        self.close_database()
        return competition_list

    def get_coach_statistic(self, coach_id: int, platform_id: int, stat_type: StatisticType = StatisticType.OVERALL, race_label: str = None,
                            tv_range: str = "all_tv", entry_name: str = None, version: int = 3):
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id, "type": stat_type, "tv_range": tv_range, "version": version}
        if stat_type in [StatisticType.LEAGUE, StatisticType.COMPETITION] and entry_name:
            query["name"] = entry_name
        if race_label is not None:
            query["overall.races_for.{}.games_played".format(race_label)] = {"$gt": 0}
            output = {"_id": 0, "overall.races_for.{}".format(race_label): 1}
        else:
            output = {"_id": 0, "monthly": 0}
        stats = collection.find_one(query, output)
        self.close_database()
        return stats

    def get_all_range_coach_statistic(self, coach_id: int, platform_id: int, stat_type: StatisticType = StatisticType.OVERALL, entry_name: str = None,
                                      version: int = 3):
        ret_stats = {}
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id, "type": stat_type, "version": version}
        if stat_type in [StatisticType.LEAGUE, StatisticType.COMPETITION] and entry_name:
            query["name"] = entry_name
        output = {"_id": 0, "monthly": 0}
        stats = collection.find(query, output)
        for stat in stats:
            ret_stats[stat.get("tv_range")] = stat
        self.close_database()
        return ret_stats

    def get_most_played(self, coach_id: int, platform_id: int, stat_type: StatisticType = StatisticType.LEAGUE, tv_range: str = "all_tv",
                        limit: int = 5, version: int = 3):
        most_played = []
        if stat_type not in [StatisticType.LEAGUE, StatisticType.COMPETITION]:
            return most_played
        self.open_database()
        collection = self.db["coaches_statistics"]
        query = {"coach_id": coach_id, "platform_id": platform_id, "type": stat_type, "tv_range": tv_range, "version": version}
        output = {"_id": 0, "monthly": 0}
        stats = collection.find(query, output).limit(limit).sort([("overall.games_played", -1)])
        for stat in stats:
            most_played.append(stat)
        self.close_database()
        return most_played

    def get_top_coaches(self, stat_type: StatisticType, platform_id: int, entry_name: str = None, minimum_match: int = 100, race_label: str = None,
                        tv_range: str = "all_tv", limit: int = 50, version: int = 3):
        top_coaches = []
        self.open_database()
        collection = self.db["coaches_statistics"]
        output = {"_id": 0, "monthly": 0}
        query = {"type": stat_type,
                 "platform_id": platform_id,
                 "tv_range": tv_range
                 }
        if stat_type in [StatisticType.LEAGUE, StatisticType.COMPETITION]:
            query["name"] = entry_name
        if race_label is not None:
            sorting = "overall.races_for.{}.win_rate".format(race_label)
            query["overall.races_for.{}.games_played".format(race_label)] = {"$gt": minimum_match - 1}
            output["overall.races_for.{}".format(race_label)] = 1
        else:
            query["overall.games_played"] = {"$gt": minimum_match - 1}
            sorting = "overall.win_rate"

        ret = collection.find(query, output).limit(limit).sort([(sorting, 1)])
        for stat in ret:
            top_coaches.append(stat)
        self.close_database()
        return top_coaches
