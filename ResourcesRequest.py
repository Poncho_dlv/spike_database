#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class ResourcesRequest:
    __dbFileName = "databases/common.db"

    @classmethod
    def add_team_emoji(cls, label, emoji_id, emoji_url):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO teamEmoji(label, emojiId, emoji_url) VALUES(?, ?, ?)""", (label, emoji_id, emoji_url,))
        conn.commit()
        conn.close()

    @classmethod
    def get_team_emoji(cls, label):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT emojiId FROM teamEmoji WHERE label=?""", (label,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return None
        else:
            return response[0]

    @classmethod
    def get_team_emoji_url(cls, label):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT emoji_url FROM teamEmoji WHERE label=?""", (label,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return None
        else:
            return response[0]

    @classmethod
    def add_casualtie_emoji(cls, casualtie_id, emoji_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (casualtie_id, emoji_id,))
        conn.commit()
        conn.close()

    @classmethod
    def get_casualtie_emoji(cls, casualtie_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT emoji_id FROM casualties WHERE casualtie_id=? """, (casualtie_id,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return casualtie_id
        else:
            return response[0]

    @classmethod
    def get_casualtie_label(cls, casualtie_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT label FROM casualties WHERE casualtie_id=? """, (casualtie_id,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return casualtie_id
        else:
            return response[0]

    @classmethod
    def add_skill_emoji(cls, skill_id, emoji_id, emoji_url):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO skillEmoji(skill_id, emoji_id, emoji_url) VALUES(?, ?, ?)""", (skill_id, emoji_id, emoji_url,))
        conn.commit()
        conn.close()

    @classmethod
    def get_skill_emoji(cls, skill_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT emoji_id FROM skillEmoji WHERE skill_id=?""", (skill_id,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return skill_id
        else:
            return response[0]

    @classmethod
    def get_skill_emoji_url(cls, skill_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT emoji_url FROM skillEmoji WHERE skill_id=?""", (skill_id,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return skill_id
        else:
            return response[0]

    @classmethod
    def get_player_type_translation(cls, type_id, lang):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT label FROM playerType WHERE type_id=? AND lang=?""", (type_id, lang,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return type_id
        else:
            return response[0]

    @classmethod
    def get_champion_name_translation(cls, type_id, lang):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT label FROM championName WHERE type_id=? AND lang=?""", (type_id, lang,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return type_id
        else:
            return response[0]

    @classmethod
    def get_race_id_with_short_name(cls, short_name):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT race_id FROM race_label WHERE label=?""", (short_name,))
        response = cursor.fetchone()
        conn.close()

        if response is None:
            return None
        else:
            return response[0]

    @classmethod
    def get_platform(cls, platform_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM platform WHERE id=?""", (platform_id,))
        response = cursor.fetchone()
        conn.close()
        return response

    @classmethod
    def get_platform_label(cls, platform_id):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT label FROM platform WHERE id=?""", (platform_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        else:
            return response[0]

    @classmethod
    def get_platform_id(cls, label):
        conn = sqlite3.connect(cls.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT id FROM platform WHERE label=?""", (label,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        else:
            return response[0]

    @classmethod
    def get_platforms(cls):
        conn = sqlite3.connect(cls.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT id FROM platform""")
        response = cursor.fetchall()
        conn.close()

        return response
