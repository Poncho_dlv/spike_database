#!/usr/bin/env python
# -*- coding: utf-8 -*-


class InvalidCoach(Exception):

    def __init__(self, coach_name: str, match_id):
        self.message = "invalid coach {} - {}".format(coach_name, match_id)


class InvalidCredential(Exception):

    def __init__(self):
        self.message = "Invalid  credential."
