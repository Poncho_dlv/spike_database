#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from enum import IntEnum

from spike_database.BaseMongoDb import BaseMongoDb


class BountyStatus(IntEnum):
    ALIVE = 0
    DEAD = 1
    FIRED = 2


class Bounty(BaseMongoDb):

    def add_or_update_bounty(self, player_data, team_data, platform_id, issuer_id, coach_name):
        self.open_database()
        collection = self.db["bounty"]
        current_date = datetime.now().strftime("%Y-%m-%d")
        query = {"player_id": player_data["id"], "platform_id": platform_id}
        new_value = {
            "$set": {
                "player_id": player_data.get("id"),
                "name": player_data.get("name"),
                "player_type": player_data.get("type"),
                "race_id": team_data.get("race_id"),
                "team_id": team_data.get("id"),
                "team_name": team_data.get("name"),
                "team_logo": team_data.get("logo"),
                "coach_id": team_data.get("coach_id"),
                "coach_name": coach_name,
                "attributes": player_data.get("attributes"),
                "skills": player_data.get("skills"),
                "casualties": player_data.get("casualties"),
                "spp": player_data.get("spp"),
                "platform_id": platform_id,
                "last_update": current_date
            }
        }
        if issuer_id is not None:
            if type(issuer_id) == int:
                new_value["$addToSet"] = {"issuers": issuer_id}
            elif type(issuer_id) == list:
                new_value["$set"]["issuers"] = issuer_id

        collection.update_one(query, new_value, upsert=True)
        query["issue_date"] = {"$exists": False}
        collection.update_one(query, {"$set": {"issue_date": datetime.now().strftime("%Y-%m-%d")}})
        self.close_database()

    def remove_bounty(self, issuer_id, player_name):
        self.open_database()
        collection = self.db["bounty"]
        query = {"name": player_name, "issuers": {"$elemMatch": {"$eq": issuer_id}}}
        new_value = {"$pull": {"issuers": issuer_id}}
        collection.update_one(query, new_value)
        self.close_database()

    def remove_bounty_by_id(self, issuer_id: int, player_id: int, platform_id: int):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id, "issuers": {"$elemMatch": {"$eq": issuer_id}}}
        new_value = {"$pull": {"issuers": issuer_id}}
        collection.update_one(query, new_value, upsert=False)
        self.close_database()

    def update_bounty_status(self, player_id, platform_id, status):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id}
        new_value = {"$set": {"status": status}}
        collection.update_one(query, new_value)
        self.close_database()

    def update_bounty_data(self, player, match, team, add_starting_skill=False):
        self.open_database()
        collection = self.db["bounty"]
        skill_list = []
        if add_starting_skill:
            skill_list.extend(player.get_starting_skills())
        skill_list.extend(player.get_skills())
        current_date = datetime.now().strftime("%Y-%m-%d")
        query = {"player_id": player.get_id(), "platform_id": match.get_platform_id()}
        new_value = {
            "$set": {
                "match_played": player.get_match_played(),
                "league_id": match.get_league_id(),
                "league_name": match.get_league_name(),
                "competition_id": match.get_competition_id(),
                "competition_name": match.get_competition_name(),
                "coach_id": team.get_coach().get_id(),
                "coach_name": team.get_coach().get_name(),
                "skills": skill_list,
                "spp": player.get_spp() + player.get_spp_gain(),
                "attributes": player.get_raw_attributes(),
                "casualties": player.get_raw_casualties_state_id(),
                "team_id": team.get_id(),
                "team_logo": team.get_logo(),
                "last_update": current_date
            }
        }
        collection.update_one(query, new_value)
        self.close_database()

    def add_badly_hurt(self, player_id, platform_id):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id}
        new_value = {"$inc": {"badly_hurt": 1}}
        collection.update_one(query, new_value)
        self.close_database()

    def add_mng(self, player_id, platform_id):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id}
        new_value = {"$inc": {"mng": 1}}
        collection.update_one(query, new_value)
        self.close_database()

    def is_bountied(self, player_id, platform_id):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id, "status": {"$nin": [1, 2]}}
        ret = collection.find_one(query, {"_id": 1})
        if ret is not None:
            self.close_database()
            return True
        self.close_database()
        return False

    def is_bountied_by_me(self, player_id, platform_id, user_id):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id, "issuers": user_id}
        ret = collection.find_one(query, {"_id": 1})
        if ret is not None:
            self.close_database()
            return True
        self.close_database()
        return False

    def get_bounty_data(self, player_id, platform_id):
        self.open_database()
        collection = self.db["bounty"]
        query = {"player_id": player_id, "platform_id": platform_id}
        ret = collection.find_one(query, {"_id": 0})
        self.close_database()
        return ret

    def get_bountied_player_in_team(self, team_id, platform_id, last_update=None):
        self.open_database()
        ret_ids = []
        collection = self.db["bounty"]
        query = {"team_id": team_id, "platform_id": platform_id, "status": {"$nin": [1, 2]}}
        if last_update is not None:
            query["last_update"] = {"$lte": last_update}
        ret = collection.find(query, {"player_id": 1})
        for x in ret:
            ret_ids.append(x["player_id"])
        self.close_database()
        return ret_ids

    def get_bountied_player_by_user(self, issuer_id):
        self.open_database()
        ret_bnty = []
        collection = self.db["bounty"]
        query = {"issuers": {"$elemMatch": {"$eq": issuer_id}}, "status": {"$nin": [1, 2]}}
        ret = collection.find(query, {"_id": 0}).sort([("platform_id", 1), ("team_name", 1), ("issue_date", -1)])
        for x in ret:
            ret_bnty.append(x)
        self.close_database()
        return ret_bnty

    def get_all_bounty(self, issuer_id):
        self.open_database()
        ret_bnty = []
        collection = self.db["bounty"]
        query = {"issuers": {"$elemMatch": {"$eq": issuer_id}}}
        ret = collection.find(query, {"_id": 0}).sort([("platform_id", 1), ("team_name", 1), ("issue_date", -1)])
        for x in ret:
            ret_bnty.append(x)
        self.close_database()
        return ret_bnty

    def get_all_active_bounty(self, issuers_id):
        self.open_database()
        ret_bnty = []
        collection = self.db["bounty"]
        query = {"issuers": {"$in": issuers_id}, "status": {"$nin": [1, 2]}}
        ret = collection.find(query, {"_id": 0}).sort([("platform_id", 1), ("team_name", 1), ("issue_date", -1)])
        for x in ret:
            ret_bnty.append(x)
        self.close_database()
        return ret_bnty

    def get_all_bounty_data(self):
        self.open_database()
        ret_bnty = []
        collection = self.db["bounty"]
        query = {}
        ret = collection.find(query, {"_id": 0, "platform_id": 1, "team_id": 1, "player_id": 1, "issuers": 1}).limit(100).sort([("last_update", 1)])
        for x in ret:
            ret_bnty.append(x)
        self.close_database()
        return ret_bnty
