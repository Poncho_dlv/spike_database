#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class Matchup(BaseMongoDb):

    def get_vs_coefficient(self, home_race, away_race):
        self.open_database()
        collection = self.db["matchup"]
        coefficient = collection.find_one({"home_team": home_race, "away_team": away_race}, {"_id": 0, "coefficient": 1})
        self.close_database()
        if coefficient is not None and "coefficient" in coefficient.keys():
            return coefficient["coefficient"]
        return None
