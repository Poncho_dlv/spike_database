#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from spike_database.BaseMongoDb import BaseMongoDb


class ImpactPlayers(BaseMongoDb):
    def set_impact_players(self, competition_id: int, platform_id: int, impact_player: list):
        self.open_database()
        collection = self.db["impact_players"]
        query = {"id": competition_id, "platform_id": platform_id}
        current_date = datetime.now().strftime("%Y-%m-%d %H:%M")
        new_value = {"$set": {"impact_player": impact_player, "update": current_date}}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_impact_players(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["impact_players"]
        standing_data = collection.find_one({"id": competition_id, "platform_id": platform_id, "impact_player": {"$exists": True}},
                                            {"_id": 0, "impact_player": 1})
        self.close_database()
        return standing_data.get("impact_player") if standing_data is not None else None
