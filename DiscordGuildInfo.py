#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing import List

from spike_database.BaseMongoDb import BaseMongoDb


class DiscordGuildInfo(BaseMongoDb):

    def update_guild_info(self, guild):
        self.open_database()
        collection = self.db["discord_guild"]
        guild["discord_id"] = str(guild["discord_id"])
        query = {"discord_id": guild["discord_id"]}
        new_value = {"$set": guild}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def remove_guilds(self, guilds_id: List[str]):
        self.open_database()
        collection = self.db["discord_guild"]
        ret = collection.delete_many({"discord_id": {"$nin": guilds_id}})
        self.close_database()
        return ret

    def get_guilds(self, guilds_id: List[str]):
        self.open_database()
        guilds = []
        collection = self.db["discord_guild"]
        query = {"discord_id": {"$in": guilds_id}}
        ret = collection.find(query)
        for x in ret:
            x['_id'] = str(x['_id'])
            guilds.append(x)
        self.close_database()
        return guilds

    def get_guild_data(self, guild_id: str):
        self.open_database()
        collection = self.db["discord_guild"]
        query = {"discord_id": guild_id}
        ret = collection.find_one(query)
        self.close_database()
        return ret

    def get_all_guild(self):
        self.open_database()
        guilds = []
        collection = self.db["discord_guild"]
        query = {}
        ret = collection.find(query, {"discord_id": 1, "icon": 1, "name": 1, "invite": 1, "premium": 1})
        for x in ret:
            guilds.append(x)
        self.close_database()
        return guilds

    def update_patreon(self, patreon_guilds):
        self.open_database()
        collection = self.db["discord_guild"]
        collection.update_many({"discord_id": {"$in": patreon_guilds}}, {"$set": {"premium": True}})
        collection.update_many({"discord_id": {"$nin": patreon_guilds}}, {"$set": {"premium": False}})
        self.close_database()
