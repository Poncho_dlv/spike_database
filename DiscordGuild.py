#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3

from spike_settings.DiscordGuildSettings import DiscordGuildSettings


class DiscordGuild:

    def __init__(self, server_id: str):
        self.__dbFileName = "databases/" + str(server_id) + ".db"
        settings = DiscordGuildSettings(server_id)
        self.__lang = settings.get_language()

    def create_tables(self):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS matches(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, cyanide_uuid TEXT)""")

        cursor.execute(
            """CREATE TABLE IF NOT EXISTS registeredLeagues(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, cyanide_id INT, name TEXT, platform_id INT, logo TEXT, UNIQUE(cyanide_id, platform_id) ON CONFLICT REPLACE)""")

        cursor.execute(
            """CREATE TABLE IF NOT EXISTS registeredCompetition(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, cyanide_id INT UNIQUE, name TEXT, channel_id INT, logo_link TEXT, emoji TEXT, platform_id INT, cyanide_league_id INT, UNIQUE(cyanide_league_id, cyanide_id, platform_id) ON CONFLICT REPLACE)""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS videoChannel(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, channel_id TEXT, channel_type INT)""")

        cursor.execute("""CREATE TABLE IF NOT EXISTS publishedVideo(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, video_id TEXT, channel_type INT)""")

        conn.commit()
        conn.close()

    def is_new_match(self, uuid):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT cyanide_uuid FROM matches WHERE cyanide_uuid=?""", (uuid,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return True
        return False

    def save_match(self, uuid):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT INTO matches(cyanide_uuid) VALUES(?)""", (uuid,))
        conn.commit()
        conn.close()

    def save_matches(self, uuids):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.executemany("""INSERT INTO matches(cyanide_uuid) VALUES(?)""", uuids)
        conn.commit()
        conn.close()

    def add_competition(self, competition_id: int, competition_name: str, channel_id: int, logo_link: str, emoji_id: str, platform_id: int,
                        cyanide_league_id: int):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute(
            """INSERT OR REPLACE INTO registeredCompetition(cyanide_id, name, channel_id, logo_link, emoji, platform_id, cyanide_league_id) VALUES(?,?,?,?,?,?,?)""",
            (competition_id, competition_name, channel_id, logo_link, emoji_id, platform_id, cyanide_league_id,))
        conn.commit()
        conn.close()

    def remove_competition(self, competition_name, platform_id: int):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""DELETE FROM registeredCompetition WHERE name=? AND platform_id=?""", (competition_name, platform_id,))
        conn.commit()
        conn.close()

    def remove_competition_in_league(self, league_id: int, platform_id: int):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        # Return data of deleted leagues
        cursor.execute("""SELECT * FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
        response = cursor.fetchall()
        cursor.execute("""DELETE FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
        conn.commit()
        conn.close()
        return response

    def remove_all_competition(self):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""DELETE FROM registeredCompetition""")
        conn.commit()
        conn.close()

    def get_reported_competitions(self, league_id=None, platform_id=None):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        request = """SELECT id FROM registeredCompetition"""
        if league_id is not None and platform_id is not None:
            request += """ WHERE cyanide_league_id={} and platform_id={}""".format(league_id, platform_id)
        elif league_id is not None:
            request += """ WHERE cyanide_league_id={}""".format(league_id)
        elif platform_id is not None:
            request += """ WHERE platform_id={}""".format(platform_id)
        cursor.execute(request)
        response = cursor.fetchall()
        conn.close()
        return response

    def get_competitions_to_report(self, league_id, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT cyanide_id FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def get_competitions_data(self, league_id, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM registeredCompetition WHERE cyanide_league_id=? AND platform_id=?""", (league_id, platform_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def get_reported_competitions_label(self, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT DISTINCT name FROM registeredCompetition WHERE platform_id=?""", (platform_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def get_competition_id(self, competition_name, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT id FROM registeredCompetition WHERE name=? AND platform_id=?""", (competition_name, platform_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        return response[0]

    def get_competition_name(self, competition_id, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT name FROM registeredCompetition WHERE cyanide_id=? AND platform_id=?""", (competition_id, platform_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        return response[0]

    def get_competition_data(self, competition_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM registeredCompetition WHERE id=?""", (competition_id,))
        response = cursor.fetchone()
        conn.close()
        return response

    def add_reported_league(self, league_name, platform_id, cyanide_id, logo):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT OR REPLACE INTO registeredLeagues(name, platform_id, cyanide_id, logo) VALUES(?,?,?,?)""",
                       (league_name, platform_id, cyanide_id, logo,))
        conn.commit()
        conn.close()

    def get_reported_leagues_label(self, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT DISTINCT name FROM registeredLeagues WHERE platform_id=?""", (platform_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def remove_reported_league(self, league_name, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        # Return data of deleted leagues
        cursor.execute("""SELECT * FROM registeredLeagues WHERE name=? AND platform_id=?""", (league_name, platform_id,))
        response = cursor.fetchall()
        cursor.execute("""DELETE FROM registeredLeagues WHERE name=? AND platform_id=?""", (league_name, platform_id,))
        conn.commit()
        conn.close()
        return response

    def get_reported_leagues(self, platform_id=None):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        request = """SELECT id FROM registeredLeagues"""
        if platform_id is not None:
            request += """ WHERE platform_id={}""".format(platform_id)
        cursor.execute(request)
        response = cursor.fetchall()
        conn.close()
        return response

    def get_leagues_to_report(self, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT DISTINCT cyanide_league_id FROM registeredCompetition WHERE platform_id=?""", (platform_id,))
        response = cursor.fetchall()
        conn.close()
        return response

    def get_league_data(self, league_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM registeredLeagues WHERE id=?""", (league_id,))
        response = cursor.fetchone()
        conn.close()
        return response

    def get_leagues_data(self):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT * FROM registeredLeagues""")
        response = cursor.fetchall()
        conn.close()
        return response

    def get_league_id(self, league_name, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT id FROM registeredLeagues WHERE name=? AND platform_id=? """, (league_name, platform_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        return response[0]

    def get_league_name(self, cyanide_id, platform_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT name FROM registeredLeagues WHERE cyanide_id=? AND platform_id=? """, (cyanide_id, platform_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return None
        return response[0]

    def add_video_channel(self, channel_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO videoChannel (channel_id, channel_type) VALUES (?, ?)""", (channel_id, 0,))
        conn.commit()
        conn.close()

    def remove_video_channel(self, channel_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""DELETE FROM videoChannel WHERE channel_type=0 AND channel_id=? """, (channel_id,))
        conn.commit()
        conn.close()

    def get_video_channel(self):
        conn = sqlite3.connect(self.__dbFileName)
        conn.row_factory = lambda cursor, row: row[0]
        cursor = conn.cursor()
        cursor.execute("""SELECT channel_id FROM videoChannel WHERE channel_type=0 ORDER BY channel_id ASC""")
        response = cursor.fetchall()
        conn.close()
        return response

    def is_new_video(self, video_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT video_id FROM publishedVideo WHERE channel_type=0 AND video_id=?""", (video_id,))
        response = cursor.fetchone()
        conn.close()
        if response is None:
            return True
        return False

    def save_video(self, video_id):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""INSERT INTO publishedVideo(video_id, channel_type) VALUES(?, ?)""", (video_id, 0,))
        conn.commit()
        conn.close()
