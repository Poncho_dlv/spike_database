#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class Coaches(BaseMongoDb):

    def add_or_update_coach(self, coach_id: int, name: str, platform_id: int, discord_id: int = None):
        # Ignore AI
        if coach_id != 0 and coach_id is not None and name != "":
            self.open_database()
            collection = self.db["coaches"]
            query = {"id": coach_id, "platform_id": platform_id}

            if discord_id is not None:
                new_value = {"$set": {"id": coach_id, "name": name, "platform_id": platform_id},
                             "$addToSet": {"discord_id": discord_id, "other_names": name}}
            else:
                new_value = {"$set": {"id": coach_id, "name": name, "platform_id": platform_id}, "$addToSet": {"other_names": name}}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()

    def link_coach(self, coach_id: int, platform_id: int, discord_id: int):
        # Ignore AI
        if coach_id != 0 and coach_id is not None:
            self.open_database()
            collection = self.db["coaches"]
            query = {"id": coach_id, "platform_id": platform_id}
            new_value = {"$set": {"id": coach_id, "platform_id": platform_id}, "$addToSet": {"discord_id": discord_id}}
            collection.update_one(query, new_value)
            self.close_database()

    def unlink_coach(self, coach_id: int, platform_id: int, discord_id: int):
        # Ignore AI
        if coach_id != 0 and coach_id is not None:
            self.open_database()
            collection = self.db["coaches"]
            query = {"id": coach_id, "platform_id": platform_id}
            new_value = {"$pull": {"discord_id": discord_id}}
            collection.update_one(query, new_value)
            self.close_database()

    def update_coach_win_rate(self, coach_id: int, platform_id: int, statistics):
        # Ignore AI
        if coach_id != 0 and coach_id is not None:
            self.open_database()
            collection = self.db["coaches"]
            query = {"id": coach_id, "platform_id": platform_id}
            new_value = {"$set": {"statistics": statistics}}
            collection.update_one(query, new_value)
            self.close_database()

    def set_twitch_link(self, discord_id: int, twitch_channel: str):
        self.open_database()
        collection = self.db["coaches"]
        query = {"discord_id": discord_id}
        new_value = {"$set": {"twitch": twitch_channel}}
        collection.update_many(query, new_value)
        self.close_database()

    def remove_twitch_link(self, discord_id: int):
        self.open_database()
        collection = self.db["coaches"]
        query = {"discord_id": discord_id}
        new_value = {"$unset": {"twitch": 1}}
        collection.update_many(query, new_value)
        self.close_database()

    def add_or_update_coach_information(self, coach_id: int = None, platform_id: int = None, name: str = None, country: str = None, lang: str = None,
                                        steam: str = None, discord_id: str = None,
                                        youtube: str = None, naf_name: str = None, naf_number: int = None):
        # Ignore AI
        if (coach_id != 0 and coach_id is not None) or discord_id is not None:
            self.open_database()
            collection = self.db["coaches"]
            values = {}
            query = {}
            if coach_id is not None:
                values["id"] = coach_id
                query["id"] = coach_id
            if platform_id is not None:
                values["platform_id"] = platform_id
                query["platform_id"] = platform_id
            if country is not None:
                values["country"] = country
            if lang is not None:
                values["lang"] = lang
            if steam is not None:
                values["steam"] = steam
            if youtube is not None:
                values["youtube"] = youtube
            if name is not None:
                values["name"] = name
            if discord_id is not None:
                query["discord_id"] = discord_id
            if naf_name is not None:
                values["naf_name"] = naf_name
            if naf_number is not None:
                values["naf_number"] = naf_number

            new_value = {"$set": values}

            if name is not None:
                new_value["$addToSet"] = {"other_names": name}

            if coach_id is not None and platform_id is not None:
                collection.update_one(query, new_value, upsert=True)
            elif discord_id is not None:
                collection.update_many(query, new_value)
            self.close_database()

    def update_videos_channel(self, name: str, platform_id: int, youtube: str = None, twitch: str = None):
        if name != "" and name is not None:
            query = {"name": name, "platform_id": platform_id}
            values = {}
            if youtube is not None:
                values["youtube"] = youtube
            if twitch is not None:
                values["twitch"] = twitch

            if len(values) > 0:
                new_value = {"$set": values}
                self.open_database()
                collection = self.db["coaches"]
                collection.update_one(query, new_value, upsert=False)
                self.close_database()

    def get_coach_data(self, coach_name: str = None, platform_id: int = None, coach_id: int = None):
        self.open_database()
        collection = self.db["coaches"]
        query = {}
        if coach_name is not None:
            query["name"] = coach_name
        if platform_id is not None:
            query["platform_id"] = platform_id
        if coach_id is not None:
            query["id"] = coach_id

        coach_data = collection.find_one(query, {"_id": 0, "statistics": 0})
        self.close_database()
        return coach_data

    def get_coach_name(self, coach_id: int, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find_one({"id": coach_id, "platform_id": platform_id}, {"_id": 0, "name": 1})
        if coach_data is not None:
            return coach_data["name"]
        self.close_database()
        return None

    def get_coaches_name(self, coaches_id: list, platform_id: int):
        ret_coach = {}
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find({"id": {"$in": coaches_id}, "platform_id": platform_id}, {"_id": 0, "id": 1, "name": 1})
        for x in coach_data:
            ret_coach[x["id"]] = x["name"]
        self.close_database()
        return ret_coach

    def get_coach_data_by_discord_id(self, discord_id: int, platform_id: int = None):
        self.open_database()
        collection = self.db["coaches"]
        query = {"discord_id": discord_id}
        if platform_id is not None:
            query["platform_id"] = platform_id
        coach_data = collection.find_one(query, {"_id": 0, "statistics": 0})
        self.close_database()
        return coach_data

    def get_coaches_data_by_discord_id(self, discord_id: int):
        ret_coach = []
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find({"discord_id": discord_id}, {"_id": 0, "statistics": 0})
        for x in coach_data:
            x["_id"] = str(x["_id"])
            ret_coach.append(x)
        self.close_database()
        return ret_coach

    def get_statistics(self, coach_id: int, platform_id: int, return_entry: list = None):
        if return_entry is None:
            return_entry = ["overall", "leagues", "competitions", "ranked_ladder", "ranked_playoffs"]
        self.open_database()
        collection = self.db["coaches"]
        return_field = {"_id": 0, "statistics.version": 1}
        for entry in return_entry:
            return_field["statistics.{}".format(entry)] = 1
        coach_data = collection.find_one({"id": coach_id, "platform_id": platform_id}, return_field)
        if coach_data is not None and coach_data.get("statistics") is not None:
            coach_data = coach_data["statistics"]
        else:
            coach_data = None
        self.close_database()
        return coach_data

    def get_deprecated_coaches(self, version: int, limit: int = 50):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find({"statistics.version": {"$ne": version}}, {"_id": 0}).limit(limit)
        self.close_database()
        return coach_data

    def get_ranked_top_coach(self, platform_id: int, minimum_match: int = 100, race_label: str = None, tv_range: str = "all_tv"):
        self.open_database()
        collection = self.db["coaches"]
        query_match_played = "statistics.ranked_ladder.{}.overall.games_played".format(tv_range)
        query = {"platform_id": platform_id, query_match_played: minimum_match}
        if race_label is not None:
            query_race_for = "statistics.ranked_ladder.{}.overall.races_for.name".format(tv_range)
            query[query_race_for] = race_label
        coach_data = collection.find(query, {"_id": 0})
        self.close_database()
        return coach_data

    def get_leagues_top_coach(self, league_name: str, platform_id: int, race_label: str = None, tv_range: str = "all_tv"):
        top_coaches = []
        self.open_database()
        collection = self.db["coaches"]
        query_league_name = "statistics.leagues.{}.overall.name".format(tv_range)
        query = {"platform_id": platform_id, query_league_name: league_name}
        if race_label is None:
            query_race_for = "statistics.leagues.{}.overall.races_for.name".format(tv_range)
            query[query_race_for] = race_label
        collection.find(query, {"_id": 0})
        self.close_database()
        return top_coaches

    def get_discord_id(self, coach_id: int, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find_one({"id": coach_id, "platform_id": platform_id}, {"discord_id": 1})
        ret = None
        if coach_data is not None and "discord_id" in coach_data.keys():
            ret = coach_data["discord_id"]
        self.close_database()
        return ret

    def coach_is_linked(self, coach_id: int, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find_one({"id": coach_id, "platform_id": platform_id, "discord_id": {"$exists": True}}, {"id": 1, "discord_id": 1})
        if coach_data is not None and len(coach_data["discord_id"]) > 0:
            self.close_database()
            return True
        self.close_database()
        return False

    def member_already_linked(self, member_id: int, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find_one({"discord_id": member_id, "platform_id": platform_id}, {"id": 1})
        if coach_data is not None:
            self.close_database()
            return True
        self.close_database()
        return False

    def coach_exist(self, coach_id: int, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_data = collection.find_one({"id": coach_id, "platform_id": platform_id}, {"id": 1})
        if coach_data is not None:
            self.close_database()
            return True
        self.close_database()
        return False

    def get_coaches_like(self, coach_name: str, platform_id: int = None, name_only: bool = True):
        ret_coaches = []
        self.open_database()
        collection = self.db["coaches"]
        query = {"name": {'$regex': "^{}".format(coach_name), "$options": "i"}}
        if platform_id is not None:
            query["platform_id"] = platform_id
        values = {"name": 1, "platform_id": 1}
        if not name_only:
            values["id"] = 1
        coaches = collection.find(query, values)
        if name_only:
            for x in coaches:
                ret_coaches.append(x["name"])
        else:
            for x in coaches:
                x["_id"] = str(x["_id"])
                ret_coaches.append(x)
        self.close_database()
        return ret_coaches

    def get_filtered_coaches_like(self, coach_name: str, limit: int = 9, platform_id: int = None):
        ret_coaches = []
        self.open_database()
        collection = self.db["coaches"]
        query = {"name": {'$regex': "{}".format(coach_name), "$options": "i"}}
        if platform_id is not None:
            query["platform_id"] = platform_id
        coaches = collection.find(query, {"_id": 0, "id": 1, "platform_id": 1, "name": 1}).limit(limit)
        for x in coaches:
            ret_coaches.append({'value': x["name"], 'data': x})
        self.close_database()
        return ret_coaches

    def get_coach_id(self, coach_name: str, platform_id: int):
        self.open_database()
        collection = self.db["coaches"]
        coach_id = collection.find_one({"name": coach_name, "platform_id": platform_id}, {"_id": 0, "id": 1})
        self.close_database()
        return coach_id["id"]
