#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class ResourcesInit:

    def __init__(self):
        self.__dbFileName = "databases/common.db"
        self.conn = None

    def open_database(self):
        if self.conn is None:
            self.conn = sqlite3.connect(self.__dbFileName)

    def close_database(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None

    def create_tables(self):
        cursor = self.conn.cursor()
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS teamEmoji(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, label TEXT UNIQUE, emojiId TEXT, emoji_url TEXT)""")
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS skillEmoji(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,skill_id TEXT, emoji_id TEXT, emoji_url TEXT)""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS platform(id INTEGER PRIMARY KEY UNIQUE, label TEXT UNIQUE, emoji_id TEXT)""")
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS casualties(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE,casualtie_id INT UNIQUE, emoji_id TEXT)""")
        cursor.execute("""CREATE TABLE IF NOT EXISTS playerType(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, type_id TEXT, label TEXT, lang TEXT)""")
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS championName(id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, type_id TEXT, label TEXT, lang TEXT)""")
        cursor.execute(
            """CREATE TABLE IF NOT EXISTS race_label (id INTEGER PRIMARY KEY AUTOINCREMENT UNIQUE, label TEXT UNIQUE, race_id INT, UNIQUE(label, race_id))""")

        cursor.execute("""CREATE UNIQUE INDEX IF NOT EXISTS idx_label_race ON race_label(label)""")

        self.conn.commit()

    def fill_tables(self):
        self.init_player_type_translation()
        self.init_champion_name_translation()
        self.init_casualties_emoji()
        self.init_platform()
        self.init_race_label()

    def init_player_type_translation(self):
        cursor = self.conn.cursor()

        # French translation
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Lineman", "Trois quart", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Catcher", "Receveur", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Thrower", "Lanceur", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Blitzer", "Blitzer", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ogre", "Ogre", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Blocker", "Bloqueur", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Runner", "Coureur", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TrollSlayer", "Tueur de Troll", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("DeathRoller", "Roule Mort", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Wardancer", "Danceur de Guerre", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Treeman", "Homme Arbre", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("GutterRunner", "Coureur d'egout", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("RatOgre", "Rat Ogre", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Goblin", "Gobelin", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BlackOrcBlocker", "Orque Noir", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Troll", "Troll", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Skink", "Skink", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Saurus", "Saurus", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Kroxigor", "Kroxigor", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Looney", "Barjot", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Beastman", "Homme Bête", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Warrior", "Guerrier du Chaos", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Minotaur", "Minotaure", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Pogoer", "Bâton à Ressort", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Fanatic", "Fanatique", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Assassin", "Assasin", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("WitchElf", "Furie", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Skeleton", "Squelette", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Zombie", "Zombie", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ghoul", "Goule", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Wight", "Revenant", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Mummy", "Momie", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Halfling", "Halfing", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Berserker", "Berseker", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ulfwerener", "Loup-Garou Nordique", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Yhetee", "Yhetee", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Linewoman", "Trois Quart", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("ThroRa", "ThrowRa", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BlitzRa", "BlitzRa", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TombGuardian", "Gardien des Tombes", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("FleshGolem", "Golem de Chair", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Werewolf", "Loup Garou", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Rotter", "Pourri", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Pestigor", "Pestigor", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("NurgleWarrior", "Guerrier de Nurgle", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BeastOfNurgle", "Bête de Nurgle", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Gnoblar", "Gnoblar", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Thrall", "Serviteur", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Vampire", "Vampire", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Bombardier", "Bombardier", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Hobgoblin", "Hobgobelin", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BullCentaur", "Centaure taureau", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TameBear", "Ours Apprivoisée", "fr",))
        # English translation
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Lineman", "Lineman", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Catcher", "Catcher", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Thrower", "Thrower", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Blitzer", "Blitzer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ogre", "Ogre", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Blocker", "Blocker", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Runner", "Runner", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TrollSlayer", "Troll Slayer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("DeathRoller", "Death Roller", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Wardancer", "Wardancer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Treeman", "Treeman", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("GutterRunner", "GutterRunner", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("RatOgre", "Rat Ogre", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Goblin", "Goblin", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BlackOrcBlocker", "Black Orc Blocker", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Troll", "Troll", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Skink", "Skink", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Saurus", "Saurus", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Kroxigor", "Kroxigor", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Looney", "Looney", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Beastman", "Beastman", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Warrior", "Warrior", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Minotaur", "Minotaur", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Pogoer", "Pogoer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Fanatic", "Fanatic", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Assassin", "Assassin", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("WitchElf", "Witch Elf", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Skeleton", "Skeleton", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Zombie", "Zombie", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ghoul", "Ghoul", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Wight", "Wight", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Mummy", "Mummy", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Halfling", "Halfling", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Berserker", "Berserker", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Ulfwerener", "Ulfwerener", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Yhetee", "Yhetee", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Linewoman", "Linewoman", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("ThroRa", "ThroRa", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BlitzRa", "BlitzRa", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TombGuardian", "Tomb Guardian", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("FleshGolem", "Flesh Golem", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Werewolf", "Werewolf", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Rotter", "Rotter", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Pestigor", "Pestigor", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("NurgleWarrior", "Nurgle Warrior", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BeastOfNurgle", "Beast Of Nurgle", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Gnoblar", "Gnoblar", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Thrall", "Thrall", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Vampire", "Vampire", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Bombardier", "Bombardier", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("Hobgoblin", "Hobgoblin", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("BullCentaur", "Bull Centaur", "en",))
        cursor.execute("""INSERT OR IGNORE INTO playerType(type_id, label, lang) VALUES(?, ?, ?)""", ("TameBear", "Tame Bear", "en",))
        self.conn.commit()

    def init_champion_name_translation(self):
        cursor = self.conn.cursor()
        # French translation
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_CHAOS", "Grashnak Blackhoof", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMAIN", "Griff Oberwald", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NAIN", "Grim Ironjaw", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKAVEN", "Headsplitter", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELFE_SYLVAIN", "Jordel Freshbreeze", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GOBELIN", "Ripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HOMME_LEZARD", "Slibli", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ORC", "Varag Ghoul Chewer", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DARK_ELF", "Horkon Heartripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORGNTHORG", "Morg N Thorg", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UNDEAD", "Count Luthorvon Drakenborg", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZARATHESLAYER", "Zara The Slayer", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SCRAPPASOREHEAD", "Scrappa Sorehead", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELDRILSIDEWINDER", "Eldril Sidewinder", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LORDBORAKTHEDESPOILER", "Lord Borak The Despoiler", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DEEPROOTSTRONGBRANCH", "Deeproot Strongbranch", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NEKBREKEREKH", "Setekh", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RAMTUTIII", "Ramtut III", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ICEPELTHAMMERBLOW", "Icepelt Hammerblow", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOMBERDRIBBLESNOT", "Bomber Dribblesnot", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZZHARGMADEYE", "Zzharg Madeye", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MIGHTYZUG", "Mighty Zug", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_JEARLICE", "J Earlice", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUBRISRAKARTH", "Hubris Rakarth", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HAKFLEMSKUTTLESPIKE", "Hakflem Skuttlespike", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BARIK", "Barik Farblast", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DOLFAR", "Dolfar Longstride", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GROTTY", "Grotty", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GLART", "Glart Smashrip Jr", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORANION", "Prince Moranion", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ROXANNA", "RoxannaDarknail", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SOAREN", "Soaren Hightower", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FEZGLITCH", "Fezglitch", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ITHACA", "Ithaca Benoin", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LEWDGRIP", "Lewdgrip Whiparm", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MAX", "Max Spleenripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_PUGGY", "Puggy Baconbreath", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKITTER", "Skitter Stab Stab", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UGROTH", "Ugroth Bolgrot", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BRICK", "Brick Farth", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_CHAOS_FALLBACK", "Grashnak Blackhoof", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMAIN_FALLBACK", "Griff Oberwald", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NAIN_FALLBACK", "Grim Ironjaw", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKAVEN_FALLBACK", "Headsplitter", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELFE_SYLVAIN_FALLBACK", "Jordel Freshbreeze", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GOBELIN_FALLBACK", "Ripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HOMME_LEZARD_FALLBACK", "Slibli", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ORC_FALLBACK", "Varag Ghoul Chewer", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DARK_ELF_FALLBACK", "Horkon Heartripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORGNTHORG_FALLBACK", "Morg N Thorg", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UNDEAD_FALLBACK", "Count Luthorvon Drakenborg", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZARATHESLAYER_FALLBACK", "Zara The Slayer", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SCRAPPASOREHEAD_FALLBACK", "Scrappa Sorehead", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELDRILSIDEWINDER_FALLBACK", "Eldril Sidewinder", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LORDBORAKTHEDESPOILER_FALLBACK", "Lord Borak The Despoiler", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DEEPROOTSTRONGBRANCH_FALLBACK", "Deeproot Strongbranch", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NEKBREKEREKH_FALLBACK", "Setekh", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RAMTUTIII_FALLBACK", "Ramtut III", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ICEPELTHAMMERBLOW_FALLBACK", "Icepelt Hammerblow", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOMBERDRIBBLESNOT_FALLBACK", "Bomber Dribblesnot", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZZHARGMADEYE_FALLBACK", "Zzharg Madeye", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MIGHTYZUG_FALLBACK", "Mighty Zug", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_JEARLICE_FALLBACK", "J Earlice", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUBRISRAKARTH_FALLBACK", "Hubris Rakarth", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HAKFLEMSKUTTLESPIKE_FALLBACK", "Hakflem Skuttlespike", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BARIK_FALLBACK", "Barik Farblast", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DOLFAR_FALLBACK", "Dolfar Longstride", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GROTTY_FALLBACK", "Grotty", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GLART_FALLBACK", "Glart Smashrip Jr", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORANION_FALLBACK", "Prince Moranion", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ROXANNA_FALLBACK", "Roxanna Darknail", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SOAREN_FALLBACK", "Soaren Hightower", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FEZGLITCH_FALLBACK", "Fezglitch", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ITHACA_FALLBACK", "Ithaca Benoin", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LEWDGRIP_FALLBACK", "Lewdgrip Whiparm", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MAX_FALLBACK", "Max Spleenripper", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_PUGGY_FALLBACK", "Puggy Baconbreath", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKITTER_FALLBACK", "Skitter Stab Stab", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UGROTH_FALLBACK", "Ugroth Bolgrot", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BRICK_FALLBACK", "Brick Farth", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HEMLOCK", "Hemlock", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HEMLOCK_FALLBACK", "Hemlock", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILHELM", "Wilhelm Chaney", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILHELM_FALLBACK", "Wilhelm Chaney", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILLOW", "Willow Rosebark", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILLOW_FALLBACK", "Willow Rosebark", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LOTTABOTTOL", "Lottabottol", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LOTTABOTTOL_FALLBACK", "Lottabottol", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_QUETZAL", "Quetzal Leap", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_QUETZAL_FALLBACK", "Quetzal Leap", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BERTHA", "Bertha Bigfist", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BERTHA_FALLBACK", "Bertha Bigfist", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RASHNAK", "Rashnak Backstabber", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RASHNAK_FALLBACK", "Rashnak Backstabber", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HTHARK_FALLBACK", "Hthark The Unstoppable", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HTHARK", "Hthark The Unstoppable", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_IGOR", "Crazy Igor", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_IGOR_FALLBACK", "Crazy Igor", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FUNGUS", "Fungus The Loon", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FUNGUS_FALLBACK", "Fungus The Loon", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NOBBLA", "Nobbla Blackwart", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NOBBLA_FALLBACK", "Nobbla Blackwart", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMERUS", "Humerus Carpal", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMERUS_FALLBACK", "Humerus Carpal", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOOMER", "Boomer Eziasson", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOOMER_FALLBACK", "Boomer Eziasson", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FLINT", "Flint Churnblade", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FLINT_FALLBACK", "Flint Churnblade", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HACK", "Hack Enslash", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HACK_FALLBACK", "Hack Enslash", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HELMUT", "Helmut Wulf", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HELMUT_FALLBACK", "Helmut Wulf", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SINNEDBAD", "Sinnedbad", "fr",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SINNEDBAD_FALLBACK", "Sinnedbad", "fr",))

        # English translation
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_CHAOS", "Grashnak Blackhoof", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMAIN", "Griff Oberwald", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NAIN", "Grim Ironjaw", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKAVEN", "Headsplitter", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELFE_SYLVAIN", "Jordel Freshbreeze", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GOBELIN", "Ripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HOMME_LEZARD", "Slibli", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ORC", "Varag Ghoul Chewer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DARK_ELF", "Horkon Heartripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORGNTHORG", "Morg N Thorg", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UNDEAD", "Count Luthorvon Drakenborg", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZARATHESLAYER", "Zara The Slayer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SCRAPPASOREHEAD", "Scrappa Sorehead", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELDRILSIDEWINDER", "Eldril Sidewinder", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LORDBORAKTHEDESPOILER", "Lord Borak The Despoiler", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DEEPROOTSTRONGBRANCH", "Deeproot Strongbranch", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NEKBREKEREKH", "Setekh", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RAMTUTIII", "Ramtut III", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ICEPELTHAMMERBLOW", "Icepelt Hammerblow", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOMBERDRIBBLESNOT", "Bomber Dribblesnot", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZZHARGMADEYE", "Zzharg Madeye", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MIGHTYZUG", "Mighty Zug", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_JEARLICE", "J Earlice", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUBRISRAKARTH", "Hubris Rakarth", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HAKFLEMSKUTTLESPIKE", "Hakflem Skuttlespike", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BARIK", "Barik Farblast", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DOLFAR", "Dolfar Longstride", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GROTTY", "Grotty", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GLART", "Glart Smashrip Jr", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORANION", "Prince Moranion", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ROXANNA", "RoxannaDarknail", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SOAREN", "Soaren Hightower", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FEZGLITCH", "Fezglitch", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ITHACA", "Ithaca Benoin", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LEWDGRIP", "Lewdgrip Whiparm", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MAX", "Max Spleenripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_PUGGY", "Puggy Baconbreath", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKITTER", "Skitter Stab Stab", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UGROTH", "Ugroth Bolgrot", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BRICK", "Brick Farth", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_CHAOS_FALLBACK", "Grashnak Blackhoof", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMAIN_FALLBACK", "Griff Oberwald", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NAIN_FALLBACK", "Grim Ironjaw", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKAVEN_FALLBACK", "Headsplitter", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELFE_SYLVAIN_FALLBACK", "Jordel Freshbreeze", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GOBELIN_FALLBACK", "Ripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HOMME_LEZARD_FALLBACK", "Slibli", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ORC_FALLBACK", "Varag Ghoul Chewer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DARK_ELF_FALLBACK", "Horkon Heartripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORGNTHORG_FALLBACK", "Morg N Thorg", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UNDEAD_FALLBACK", "Count Luthorvon Drakenborg", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZARATHESLAYER_FALLBACK", "Zara The Slayer", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SCRAPPASOREHEAD_FALLBACK", "Scrappa Sorehead", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ELDRILSIDEWINDER_FALLBACK", "Eldril Sidewinder", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LORDBORAKTHEDESPOILER_FALLBACK", "Lord Borak The Despoiler", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DEEPROOTSTRONGBRANCH_FALLBACK", "Deeproot Strongbranch", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NEKBREKEREKH_FALLBACK", "Setekh", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RAMTUTIII_FALLBACK", "Ramtut III", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ICEPELTHAMMERBLOW_FALLBACK", "Icepelt Hammerblow", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOMBERDRIBBLESNOT_FALLBACK", "Bomber Dribblesnot", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ZZHARGMADEYE_FALLBACK", "Zzharg Madeye", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MIGHTYZUG_FALLBACK", "Mighty Zug", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_JEARLICE_FALLBACK", "J Earlice", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUBRISRAKARTH_FALLBACK", "Hubris Rakarth", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HAKFLEMSKUTTLESPIKE_FALLBACK", "Hakflem Skuttlespike", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BARIK_FALLBACK", "Barik Farblast", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_DOLFAR_FALLBACK", "Dolfar Longstride", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GROTTY_FALLBACK", "Grotty", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_GLART_FALLBACK", "Glart Smashrip Jr", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MORANION_FALLBACK", "Prince Moranion", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ROXANNA_FALLBACK", "Roxanna Darknail", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SOAREN_FALLBACK", "Soaren Hightower", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FEZGLITCH_FALLBACK", "Fezglitch", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_ITHACA_FALLBACK", "Ithaca Benoin", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LEWDGRIP_FALLBACK", "Lewdgrip Whiparm", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_MAX_FALLBACK", "Max Spleenripper", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_PUGGY_FALLBACK", "Puggy Baconbreath", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SKITTER_FALLBACK", "Skitter Stab Stab", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_UGROTH_FALLBACK", "Ugroth Bolgrot", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BRICK_FALLBACK", "Brick Farth", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HEMLOCK", "Hemlock", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HEMLOCK_FALLBACK", "Hemlock", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILHELM", "Wilhelm Chaney", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILHELM_FALLBACK", "Wilhelm Chaney", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILLOW", "Willow Rosebark", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_WILLOW_FALLBACK", "Willow Rosebark", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LOTTABOTTOL", "Lottabottol", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_LOTTABOTTOL_FALLBACK", "Lottabottol", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_QUETZAL", "Quetzal Leap", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_QUETZAL_FALLBACK", "Quetzal Leap", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BERTHA", "Bertha Bigfist", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BERTHA_FALLBACK", "Bertha Bigfist", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RASHNAK", "Rashnak Backstabber", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_RASHNAK_FALLBACK", "Rashnak Backstabber", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HTHARK_FALLBACK", "Hthark The Unstoppable", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HTHARK", "Hthark The Unstoppable", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_IGOR", "Crazy Igor", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_IGOR_FALLBACK", "Crazy Igor", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FUNGUS", "Fungus The Loon", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FUNGUS_FALLBACK", "Fungus The Loon", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NOBBLA", "Nobbla Blackwart", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_NOBBLA_FALLBACK", "Nobbla Blackwart", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMERUS", "Humerus Carpal", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HUMERUS_FALLBACK", "Humerus Carpal", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOOMER", "Boomer Eziasson", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_BOOMER_FALLBACK", "Boomer Eziasson", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FLINT", "Flint Churnblade", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_FLINT_FALLBACK", "Flint Churnblade", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HACK", "Hack Enslash", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HACK_FALLBACK", "Hack Enslash", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HELMUT", "Helmut Wulf", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_HELMUT_FALLBACK", "Helmut Wulf", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SINNEDBAD", "Sinnedbad", "en",))
        cursor.execute("""INSERT OR IGNORE INTO championName(type_id, label, lang) VALUES(?, ?, ?)""",
                       ("PLAYER_NAMES_CHAMPION_SINNEDBAD_FALLBACK", "Sinnedbad", "en",))
        self.conn.commit()

    def init_casualties_emoji(self):
        cursor = self.conn.cursor()

        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (1, "",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (2, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (3, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (4, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (5, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (6, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (7, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (8, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (9, "<:MNG:477022957923860490>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (10, "<:Niggle:477022957655556106>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (11, "<:Niggle:477022957655556106>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (12, "<:DecreaseMovement:477022957781385218>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (13, "<:DecreaseMovement:477022957781385218>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (14, "<:DecreaseArmour:477022957693042689>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (15, "<:DecreaseArmour:477022957693042689>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (16, "<:DecreaseAgility:477022957558956043>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (17, "<:DecreaseStrength:477022958079049728>",))
        cursor.execute("""INSERT OR IGNORE INTO casualties(casualtie_id, emoji_id) VALUES(?, ?)""", (18, "<:Dead:477022957391314955>",))
        self.conn.commit()

    def init_platform(self):
        cursor = self.conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO platform(id, label, emoji_id) VALUES(?, ?, ?)""", (1, "pc", "<:pc:470240774085410816>",))
        cursor.execute("""INSERT OR IGNORE INTO platform(id, label, emoji_id) VALUES(?, ?, ?)""", (2, "ps4", "<:ps4:470240774353846285>",))
        cursor.execute("""INSERT OR IGNORE INTO platform(id, label, emoji_id) VALUES(?, ?, ?)""", (3, "xb1", "<:xb1:470240774475218944>",))
        self.conn.commit()

    def init_race_label(self):
        cursor = self.conn.cursor()
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("hum", 1))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("dorf", 2))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("nain", 2))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("rats", 3))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("ork", 4))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("lez", 5))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("hl", 5))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("woodies", 7))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("we", 7))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("chaos", 8))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("de", 9))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("ud", 10))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("flings", 11))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("fling", 11))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("zons", 13))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("zon", 13))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("eu", 14))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("he", 15))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("vamps", 20))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("chorfs", 21))
        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("chorf", 21))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("uw", 22))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("brets", 24))

        cursor.execute("""INSERT OR IGNORE INTO race_label(label, race_id) VALUES(?,?)""", ("slann", 25))

        self.conn.commit()
