#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import errors

from spike_database.BaseMongoDb import BaseMongoDb


class Matches(BaseMongoDb):

    def add_match(self, match_uuid, json_data):
        try:
            self.open_database()
            collection = self.db["matches"]
            query = {"_id": match_uuid}
            new_value = {"$set": {"match_data": json_data}}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()
        except errors.DuplicateKeyError:
            self.close_database()
            pass  # Ignore Already existing match

    def get_match_data(self, match_uuid):
        self.open_database()
        collection = self.db["matches"]
        query = {"_id": match_uuid}
        match_data = collection.find_one(query, {"_id": 0, "match_data": 1})
        if match_data is not None and "match_data" in match_data.keys():
            match_data = match_data["match_data"]
        self.close_database()
        return match_data

    def get_matches_in_competition(self, competition_id: int, platform: str):
        ret = []
        self.open_database()
        collection = self.db["matches"]
        match_data = collection.find({"match_data.match.idcompetition": competition_id, "match_data.match.platform": platform},
                                     {"_id": 0, "match_data": 1})
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret

    def get_match_unsaved_in_match_history(self):
        ret = []
        self.open_database()
        collection = self.db["matches"]
        match_data = collection.find({"match_history": {"$ne": 1}, "match_data.match": {"$ne": None}}, {"_id": 0, "match_data": 1}).limit(500)
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret

    def get_match_unsaved_in_contests(self):
        ret = []
        self.open_database()
        collection = self.db["matches"]
        match_data = collection.find({"contest_updated": {"$ne": 2}, "match_data.match": {"$ne": None}}, {"_id": 0, "match_data": 1}).limit(500)
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret

    def get_match_to_check_for_bounty(self):
        ret = []
        self.open_database()
        collection = self.db["matches"]
        match_data = collection.find({"bounty_checked": {"$ne": 1}, "match_data.match": {"$ne": None}}, {"_id": 0, "match_data": 1}).limit(500)
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret

    def get_match_to_check_for_update_team_and_player(self):
        ret = []
        self.open_database()
        collection = self.db["matches"]
        match_data = collection.find({"team_player_updated": {"$ne": 1}, "match_data.match": {"$ne": None}}, {"_id": 0, "match_data": 1}).limit(500)
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret

    def set_match_saved_in_match_history(self, ids):
        self.open_database()
        collection = self.db["matches"]
        collection.update_many({"_id": {"$in": ids}}, {"$set": {"match_history": 1}})
        self.close_database()

    def set_match_checked_for_bounty(self, ids):
        self.open_database()
        collection = self.db["matches"]
        collection.update_many({"_id": {"$in": ids}}, {"$set": {"bounty_checked": 1}})
        self.close_database()

    def set_match_saved_in_contests(self, ids):
        self.open_database()
        collection = self.db["matches"]
        collection.update_many({"_id": {"$in": ids}}, {"$set": {"contest_updated": 2}})
        self.close_database()

    def set_match_to_check_for_update_team_and_player(self, ids):
        self.open_database()
        collection = self.db["matches"]
        collection.update_many({"_id": {"$in": ids}}, {"$set": {"team_player_updated": 1}})
        self.close_database()

    def get_last_match_in_league(self, league_id: int, platform: str, limit: int):
        ret_data = []
        self.open_database()
        collection = self.db["matches"]
        query = {"match_data.match.idleague": league_id, "match_data.match.platform": platform}
        data = collection.find(query).limit(limit).sort([("match_data.match.finished", -1)])
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_matches_in_league(self, league_id: int, platform: str):
        ret_data = []
        self.open_database()
        collection = self.db["matches"]
        query = {"match_data.match.idleague": league_id, "match_data.match.platform": platform}
        data = collection.find(query)
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def remove_match(self, match_uuid: str):
        self.open_database()
        collection = self.db["matches"]
        query = {"_id": match_uuid}
        collection.delete_one(query)
        self.close_database()

    def set_corrupted_data(self, match_uuid: str, corrupted: bool = True):
        self.open_database()
        collection = self.db["matches"]
        query = {"_id": match_uuid}
        new_value = {"$set": {"data_corrupted": corrupted}}
        collection.update_one(query, new_value, upsert=False)
        self.close_database()
