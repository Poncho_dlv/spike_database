#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class BaseDatabase:

    def __init__(self, file_name):
        self.file_name = file_name
        self.conn = None

    def open_database(self):
        if self.conn is None:
            self.conn = sqlite3.connect(self.file_name)

    def close_database(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None
