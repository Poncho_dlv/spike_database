#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import errors

from spike_database.BaseMongoDb import BaseMongoDb


class MatchHistory(BaseMongoDb):

    def add_or_update_match(self, match_uuid: str, match_data):
        try:
            self.open_database()
            collection = self.db['match_history']
            query = {'_id': match_uuid}
            new_value = {'$set': match_data}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()
        except errors.DuplicateKeyError:
            self.close_database()
            pass  # Ignore Already existing match

    def insert_matches(self, matches: list):
        self.open_database()
        collection = self.db['match_history']
        collection.insert_many(matches)
        self.close_database()

    def match_exist(self, match_uuid: str):
        self.open_database()
        collection = self.db['match_history']
        query = {'_id': match_uuid}
        ret = collection.find_one(query, {'_id': 1})
        ret = False
        if ret is not None:
            ret = True
        self.close_database()
        return ret

    def matches_exist(self, match_uuid: list):
        self.open_database()
        collection = self.db['match_history']
        ret_data = []
        query = {'_id': {'$in': match_uuid}}
        data = collection.find(query, {'_id': 1})
        for x in data:
            ret_data.append(x['_id'])
        self.close_database()
        return ret_data

    def get_vs_history(self, coach1_id: int, coach2_id: int, platform_id: int, competition_name=None, league_name=None):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id,
                 'duration': {'$ne': 0},
                 '$and': [
                     {'$or': [{'team_away.coach_id': coach1_id}, {'team_home.coach_id': coach1_id}]},
                     {'$or': [{'team_away.coach_id': coach2_id}, {'team_home.coach_id': coach2_id}]}
                 ]
                 }
        if competition_name is not None:
            query['competition_name'] = competition_name
        if league_name is not None:
            query['league_name'] = league_name
        data = collection.find(query)
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_match_history(self, coach_id: int, platform_id: int, competition_name=None, league_name=None, race_id=None):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id,
                 'duration': {'$ne': 0}
                 }
        if competition_name is not None:
            query['competition_name'] = competition_name
        if league_name is not None:
            query['league_name'] = league_name
        if race_id is not None:
            query['$or'] = [{'team_away.coach_id': coach_id, 'team_away.race_id': race_id},
                            {'team_home.coach_id': coach_id, 'team_home.race_id': race_id}]
        else:
            query['$or'] = [{'team_away.coach_id': coach_id}, {'team_home.coach_id': coach_id}]
        data = collection.find(query, {'_id': 0})
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_last_match(self, coach_id: int, platform_id: int, limit: int):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id, 'duration': {'$ne': 0}, '$or': [{'team_away.coach_id': coach_id}, {'team_home.coach_id': coach_id}]}
        data = collection.find(query).limit(limit).sort([('date', -1)])
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_last_match_in_league(self, league_name: str, platform_id: int, limit: int):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id, 'duration': {'$ne': 0}, 'league_name': league_name}
        data = collection.find(query).limit(limit).sort([('date', -1)])
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_last_match_in_league_by_id(self, league_id: int, platform_id: int, limit: int):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id, 'league_id': league_id}
        data = collection.find(query).limit(limit).sort([('date', -1)])
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_match_history_by_league(self, league_name: str, platform_id: int, competition_name=None):
        ret_data = []
        self.open_database()
        collection = self.db['match_history']
        query = {'platform': platform_id,
                 'duration': {'$ne': 0}
                 }
        if competition_name is not None:
            query['competition_name'] = competition_name
        if league_name is not None:
            query['league_name'] = league_name

        data = collection.find(query, {'_id': 0})
        for x in data:
            ret_data.append(x)
        self.close_database()
        return ret_data

    def get_match_data(self, league_name: str, competition_name: str, platform_id: int, home_team: str, away_team: str):
        self.open_database()
        collection = self.db['match_history']
        query = {'league_name': league_name, 'competition_name': competition_name, 'platform': platform_id, 'team_home.name': home_team,
                 'team_away.name': away_team}
        match_data = collection.find_one(query)
        self.close_database()
        return match_data

    def get_match_to_update(self, version: int = 1):
        ret = []
        self.open_database()
        collection = self.db['match_history']
        match_data = collection.find({'version': {'$ne': version}, 'full_data_not_found': {'$ne': True}}).limit(500)
        for x in match_data:
            ret.append(x)
        self.close_database()
        return ret

    def ignore_match(self, match_uuid: str, ignore: bool = True):
        self.open_database()
        collection = self.db['match_history']
        query = {'_id': match_uuid}
        new_value = {'$set': {'ignore': ignore}}
        collection.update_one(query, new_value)
        self.close_database()

    def set_full_data_not_found(self, match_uuid: str, not_found: bool = True):
        self.open_database()
        collection = self.db['match_history']
        query = {'_id': match_uuid}
        new_value = {'$set': {'full_data_not_found': not_found}}
        collection.update_one(query, new_value)
        self.close_database()
