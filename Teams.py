#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class Teams(BaseMongoDb):

    def add_or_update_team(self, team, platform_id):
        self.open_database()
        collection = self.db["teams"]
        query = {"id": team.get_id(), "platform_id": platform_id}
        new_value = {
            "$set":
                {
                    "id": team.get_id(),
                    "name": team.get_name(),
                    "platform_id": platform_id,
                    "race_id": team.get_race_id(),
                    "logo": team.get_logo(),
                    "coach_id": team.get_coach().get_id(),
                    "created": team.get_creation_date(),
                    "last_match": team.get_date_last_match(),
                    "color": team.get_team_color(),
                    "leitmotiv": team.get_motto(),
                    "value": team.get_team_value(),
                    "popularity": team.get_popularity(),
                    "cash": team.get_cash(),
                    "cheerleaders": team.get_nb_cheerleaders(),
                    "balms": team.get_balms(),
                    "apothecary": team.get_apothecary(),
                    "rerolls": team.get_nb_rerolls(),
                    "assistant_coaches": team.get_nb_assistant(),
                    "stadium_name": team.get_stadium_name(),
                    "stadium_level": team.get_stadium_level(),
                    "stadium_type": team.get_stadium_type()
                }
        }
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_team(self, team_id, platform_id):
        self.open_database()
        collection = self.db["teams"]
        team__data = collection.find_one({"id": team_id, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return team__data
