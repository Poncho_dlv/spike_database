#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.DiscordGuild import DiscordGuild


class PrivateMessage(DiscordGuild):

    def __init__(self):
        super(PrivateMessage, self).__init__(0)
