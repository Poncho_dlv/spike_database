#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class TopCoaches(BaseMongoDb):

    def save_league_top_coaches(self, league_name: str, platform_id: int, overall_top_coach_rank: dict, race_top_coach_rank: dict):
        self.open_database()
        collection = self.db["top_coaches"]
        query = {"league_name": league_name, "platform_id": platform_id}
        league_data = {"league_name": league_name, "platform_id": platform_id, "overall_top_coach_rank": overall_top_coach_rank,
                       "race_top_coach_rank": race_top_coach_rank}

        new_value = {"$set": league_data}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_league_top_coaches(self, league_name: str, platform_id: int, tv_range: str = "all_tv"):
        self.open_database()
        collection = self.db["top_coaches"]
        query = {"league_name": league_name, "platform_id": platform_id}
        standing_data = collection.find_one(query, {"_id": 0, "overall_top_coach_rank.{}".format(tv_range): 1})
        self.close_database()
        return standing_data
