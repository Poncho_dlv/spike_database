#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pymongo

from spike_database.Exception import InvalidCredential
from spike_settings.SpikeSettings import SpikeSettings


class BaseMongoDb:

    def __init__(self):
        self.client = None
        self.db = None

    def open_database(self):
        if self.client is None or self.db is None:
            db_user = SpikeSettings.get_database_user()
            db_password = SpikeSettings.get_database_password()
            db_host = SpikeSettings.get_database_host()

            if db_host is not None:
                if db_user is not None and db_password is not None:
                    self.client = pymongo.MongoClient("mongodb://{}:{}@{}/Spike".format(db_user, db_password, db_host), connect=False)
                else:
                    self.client = pymongo.MongoClient("mongodb://{}/Spike".format(db_host), connect=False)
                self.db = self.client["Spike"]
            else:
                raise InvalidCredential

    def close_database(self):
        if self.client is not None:
            self.client.close()
            self.client = None
            self.db = None
