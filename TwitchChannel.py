#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum

from spike_database.BaseMongoDb import BaseMongoDb


class ChannelStatus(IntEnum):
    OFFLINE = 0
    LIVE = 1


class TwitchChannel(BaseMongoDb):
    def register_twitch_channel(self, twitch_channel_id: int, display_name: str, user_name: str, discord_guild: int):
        self.open_database()
        collection = self.db["twitch_channel"]
        query = {"channel_id": twitch_channel_id}
        new_value = {"$set": {"channel_id": twitch_channel_id, "display_name": display_name, "user_name": user_name},
                     "$addToSet": {"discord_guild": discord_guild}}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def unregister_twitch_channel(self, twitch_chanel_id, discord_guild):
        self.open_database()
        collection = self.db["twitch_channel"]
        query = {"channel_id": twitch_chanel_id}
        new_value = {"$pull": {"discord_guild": discord_guild}}
        collection.update_one(query, new_value)
        self.close_database()

    def set_channel_status(self, twitch_channel_id: int, status: int, started_at=None):
        self.open_database()
        collection = self.db["twitch_channel"]
        query = {"channel_id": twitch_channel_id}
        new_value = {"$set": {"status": status}}
        if started_at is not None:
            new_value["$set"]["started_at"] = started_at
        collection.update_one(query, new_value)
        self.close_database()

    def get_registered_channel(self, discord_guild=None):
        self.open_database()
        registered_channel = []
        collection = self.db["twitch_channel"]
        if discord_guild is not None:
            query = {"discord_guild": discord_guild}
        else:
            query = {}
        ret = collection.find(query, {"_id": 0, })
        for x in ret:
            registered_channel.append(x)
        self.close_database()
        return registered_channel
