#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from typing import Dict

from bson.objectid import ObjectId

from spike_database.BaseMongoDb import BaseMongoDb


class Contests(BaseMongoDb):

    def add_or_update_contest(self, contest, platform_id: int, max_round: int = None, competition_logo: int = None, channel_id: int = None,
                              message_id: int = None):
        self.open_database()
        collection = self.db["contests"]
        current_date = datetime.utcnow().isoformat()
        query = {"contest_id": contest.get_contest_id(), "platform_id": platform_id}

        # Get old score and keep it in case of match played but not validated by admin
        old_score = collection.find_one(query, {"team_home.score": 1, "team_away.score": 1, "_id": 0})

        new_value = {"$set": {
            "contest_id": contest.get_contest_id(),
            "platform_id": platform_id,
            "league_name": contest.get_league_name(),
            "league_id": contest.get_league_id(),
            "competition_name": contest.get_competition_name(),
            "competition_id": contest.get_competition_id(),
            "format": contest.get_format(),
            "current_round": contest.get_round(),
            "status": contest.get_status(),
            "last_update": current_date,
            "team_home": {
                "team_name": contest.get_team_home().get_name(),
                "team_id": contest.get_team_home().get_id(),
                "team_logo": contest.get_team_home().get_logo(),
                "team_value": contest.get_team_home().get_team_value(),
                "race": contest.get_team_home().get_race(),
                "coach_name": contest.get_coach_home().get_name(),
                "coach_id": contest.get_coach_home().get_id()
            },
            "team_away": {
                "team_name": contest.get_team_away().get_name(),
                "team_id": contest.get_team_away().get_id(),
                "team_logo": contest.get_team_away().get_logo(),
                "team_value": contest.get_team_away().get_team_value(),
                "race": contest.get_team_away().get_race(),
                "coach_name": contest.get_coach_away().get_name(),
                "coach_id": contest.get_coach_away().get_id()
            }
        }}

        if max_round is not None:
            new_value["$set"]["max_round"] = max_round
        if competition_logo is not None:
            new_value["$set"]["competition_logo"] = competition_logo

        if contest.get_status() == 2:
            if contest.get_coach_winner() is not None:
                new_value["$set"]["winner"] = {
                    "coach_id": contest.get_coach_winner().get_id(),
                    "team_id": contest.get_team_winner().get_id()
                }
            new_value["$set"]["team_home"]["score"] = contest.data["opponents"][0]["team"]["score"]
            new_value["$set"]["team_away"]["score"] = contest.data["opponents"][1]["team"]["score"]
            if new_value.get("$addToSet") is None:
                new_value["$addToSet"] = {}
            new_value["$addToSet"]["match_uuid"] = contest.get_match_uuid()
        elif old_score is not None:
            if old_score.get("team_home", {}).get("score") is not None:
                new_value["$set"]["team_home"]["score"] = old_score.get("team_home", {}).get("score")
            if old_score.get("team_away", {}).get("score") is not None:
                new_value["$set"]["team_away"]["score"] = old_score.get("team_away", {}).get("score")

        if channel_id is not None and message_id is not None:
            if new_value.get("$addToSet") is None:
                new_value["$addToSet"] = {}
            new_value["$addToSet"]["messages"] = {"channel_id": channel_id, "message_id": message_id}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def update_contest_date(self, contest_id: int, platform_id: int, date: str):
        self.open_database()
        collection = self.db["contests"]
        query = {"contest_id": contest_id, "platform_id": platform_id}
        collection.update_one(query, {"$set": {"date": date}}, upsert=False)
        self.close_database()

    def update_contest_from_match(self, match):
        self.open_database()
        collection = self.db["contests"]
        current_date = datetime.utcnow().isoformat()

        query = {
            "league_id": match.get_league_id(),
            "competition_id": match.get_competition_id(),
            "team_home.coach_id": match.get_coach_home().get_id(),
            "team_away.coach_id": match.get_coach_away().get_id(),
            "platform_id": match.get_platform_id(),
            "current_round": match.get_round()
        }

        new_value = {
            "$set": {
                "status": 2,
                "last_update": current_date,
                "team_home.score": match.get_score_home(),
                "team_home.team_id": match.get_team_home().get_id(),
                "team_home.race": match.get_team_home().get_race(),
                "team_home.team_value": match.get_team_home().get_team_value(),
                "team_away.score": match.get_score_away(),
                "team_away.team_id": match.get_team_away().get_id(),
                "team_away.race": match.get_team_away().get_race(),
                "team_away.team_value": match.get_team_away().get_team_value()
            },
            "$addToSet": {"match_uuid": match.get_uuid()}
        }

        if match.get_winner_coach() is not None:
            new_value["$set"]["winner"] = {}
            new_value["$set"]["winner"]["coach_id"] = match.get_winner_coach().get_id()
            new_value["$set"]["winner"]["team_id"] = match.get_winner_team().get_id()

        # upsert False because of no contest_id (index)
        collection.update_one(query, new_value, upsert=False)
        self.close_database()

    def get_contest_data(self, message_id: int):
        self.open_database()
        collection = self.db["contests"]
        query = {"messages.message_id": message_id}
        ret = collection.find_one(query)
        self.close_database()
        return ret

    def get_contest_by_id(self, contest_id: str):
        self.open_database()
        collection = self.db["contests"]
        query = {"_id": ObjectId(contest_id)}
        ret = collection.find_one(query)
        self.close_database()
        return ret

    def get_contest_data_with_coach(self, coach_id: int, platform_id: int):
        self.open_database()
        contests = []
        collection = self.db["contests"]
        query = {"platform_id": platform_id, "match_uuid": {"$exists": False},
                 "$or": [{"team_away.coach_id": coach_id}, {"team_home.coach_id": coach_id}]}
        ret = list(collection.find(query))
        for x in ret:
            contests.append(x)
        self.close_database()
        return contests

    def get_contest_in_competitions(self, league_competition_data):
        if len(league_competition_data) > 0:
            self.open_database()
            contests = []
            collection = self.db["contests"]
            query = {"match_uuid": {"$exists": False}, "date": {"$exists": True}, "$or": []}
            for entry in league_competition_data:
                query["$or"].append({"league_id": entry["league_id"], "competition_id": entry["competition_id"], "platform_id": entry["platform_id"]})
            ret = collection.find(query)
            for x in ret:
                contests.append(x)
            self.close_database()
            return contests
        return None

    def get_contests_in_competition(self, competition_id: int, platform_id: int, status=None, round_index=None):
        self.open_database()
        contests = []
        collection = self.db["contests"]
        query = {"competition_id": competition_id, "platform_id": platform_id}
        if status is not None:
            query["status"] = status
        if round_index is not None:
            query["current_round"] = round_index
        ret = collection.find(query)
        for x in ret:
            contests.append(x)
        self.close_database()
        return contests

    def get_contest_date(self, contest_id: int, platform_id: int):
        self.open_database()
        collection = self.db["contests"]
        query = {"contest_id": contest_id, "platform_id": platform_id}
        ret = collection.find_one(query, {"_id": 0, "date": 1})
        if ret is not None and "date" in ret.keys():
            ret = ret["date"]
        self.close_database()
        return ret

    def get_contest_messages(self, league_id: int, competition_id: int, coach_home_id: int, coach_away_id: int, platform_id: int):
        self.open_database()
        collection = self.db["contests"]
        query = {"league_id": league_id, "competition_id": competition_id, "team_home.coach_id": coach_home_id, "team_away.coach_id": coach_away_id,
                 "platform_id": platform_id}
        ret = collection.find_one(query, {"_id": 0, "messages": 1})
        if ret is not None and "messages" in ret.keys():
            ret = ret["messages"]
        self.close_database()
        return ret

    def get_contest_to_register(self):
        self.open_database()
        contests = []
        collection = self.db["contests"]
        ret = collection.find({"match_uuid": {"$exists": False}, "date": {"$exists": True}}, {"_id": 0}).limit(500)
        for x in ret:
            contests.append(x)
        self.close_database()
        return ret

    def remove_contest(self, contest_id: int, platform_id: int):
        self.open_database()
        collection = self.db["contests"]
        query = {"contest_id": contest_id, "platform_id": platform_id}
        collection.update_one(query, {"$addToSet": {"match_uuid": None}}, upsert=False)
        self.close_database()

    def get_odds(self, contest_id: int, platform_id: int):
        self.open_database()
        collection = self.db["contests"]
        query = {"contest_id": contest_id, "platform_id": platform_id}
        ret = collection.find_one(query, {"_id": 0, "odds": 1})
        if ret is not None and "odds" in ret.keys():
            ret = ret["odds"]
        else:
            ret = None
        self.close_database()
        return ret

    def set_odds(self, contest_id: int, platform_id: int, odds: Dict[str, float]):
        self.open_database()
        collection = self.db["contests"]
        query = {"contest_id": contest_id, "platform_id": platform_id}
        collection.update_one(query, {"$set": {"odds": odds}})
        self.close_database()
