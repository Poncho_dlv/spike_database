#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import sqlite3

from spike_database.Coaches import Coaches
from spike_database.Exception import InvalidCoach
from spike_database.MatchHistory import MatchHistory
from spike_database.ResourcesRequest import ResourcesRequest

spike_logger = logging.getLogger("spike_logger")


class GoblinSpy:

    def __init__(self, db_name):
        self.__dbFileName = "goblin_spy_data/www.mordrek.com/goblinSpy/Database/" + db_name
        self.conn = None
        self.__version = None
        self.open_database()
        self.__check_db_version()
        self.close_database()

    def open_database(self):
        if self.conn is None:
            self.conn = sqlite3.connect(self.__dbFileName)

    def close_database(self):
        if self.conn is not None:
            self.conn.close()
            self.conn = None

    def __check_db_version(self):
        cursor = self.conn.cursor()

        try:
            cursor.execute("""SELECT name FROM sqlite_master WHERE type = "table" AND name = "leaguematches" """)
            response = cursor.fetchone()
            if response is not None:
                try:
                    cursor.execute("""SELECT lmver FROM leaguematches LIMIT 1""")
                    response = cursor.fetchone()
                    if response is not None:
                        self.__version = response[0]
                    else:
                        spike_logger.info("Empty database: {}".format(self.__dbFileName))
                except sqlite3.OperationalError:
                    try:
                        cursor.execute("""SELECT ver FROM leaguematches LIMIT 1""")
                        response = cursor.fetchone()
                        if response is not None:
                            self.__version = response[0]
                        else:
                            spike_logger.info("Empty database: {}".format(self.__dbFileName))
                    except sqlite3.OperationalError:
                        spike_logger.error("Unknown db version: {}".format(self.__dbFileName))
            else:
                spike_logger.info("No table leaguematches in:{}".format(self.__dbFileName))
        except sqlite3.DatabaseError:
            spike_logger.error("Database disk image is malformed : {}".format(self.__dbFileName))

    def write_match_data(self):
        if self.__version is None:
            pass
        elif self.__version == 9 or self.__version == 11:
            self.write_match_data_v1()
        elif self.__version > 13:
            self.write_match_data_v2()
        else:
            spike_logger.error("Unknown db version: {}".format(self.__dbFileName))

    def write_match_data_v1(self):
        try:
            self.open_database()
            cursor = self.conn.cursor()

            cursor.execute("""SELECT uuid, competitionname, leaguename, platform, date, duration, coachhome, 
            idcoachstatshome, idteamstatshome, coachaway, idcoachstatsaway, idteamstatsaway, concededhome, idracehome, 
            logohome, teamhome, scorehome, scoreaway, teamaway, logoaway, idraceaway, concededaway FROM leaguematches""")

            response = cursor.fetchall()

            if response is not None:
                spike_logger.info("Save history for: \"{}\" - \"{}\" - \"{}\"".format(response[0][2], response[0][1], response[0][3]))
                match_history_db = MatchHistory()
                coach_db = Coaches()
                rsrc_db = ResourcesRequest()
                platform_id = rsrc_db.get_platform_id(response[0][3])

                for match in response:
                    id_team_stats_home = match[8]
                    cursor.execute("""SELECT value FROM teamstats WHERE idteamstats=?""", (id_team_stats_home,))
                    tv_home = cursor.fetchone()
                    if tv_home is not None:
                        tv_home = tv_home[0]
                    else:
                        spike_logger.error("No matching in teamstats for {}".format(id_team_stats_home))

                    id_team_stats_away = match[11]
                    cursor.execute("""SELECT value FROM teamstats WHERE idteamstats=?""", (id_team_stats_away,))
                    tv_away = cursor.fetchone()
                    if tv_away is not None:
                        tv_away = tv_away[0]
                    else:
                        spike_logger.error("No matching in teamstats for {}".format(id_team_stats_away))

                    try:
                        coach_home_id = int(match[7].split("-")[1])
                        coach_db.add_or_update_coach(coach_home_id, match[6], platform_id)
                    except:
                        coach_home_data = coach_db.get_coach_data(coach_name=match[6], platform_id=platform_id)
                        if coach_home_data is not None:
                            coach_home_id = coach_home_data["id"]
                        else:
                            raise InvalidCoach(match[6], match[0])

                    try:
                        coach_away_id = int(match[10].split("-")[1])
                        coach_db.add_or_update_coach(coach_away_id, match[9], platform_id)
                    except:
                        coach_away_data = coach_db.get_coach_data(coach_name=match[9], platform_id=platform_id)
                        if coach_away_data is not None:
                            coach_away_id = coach_away_data["id"]
                        else:
                            raise InvalidCoach(match[9], match[0])

                    match_data = {
                        "_id": match[0],
                        "competition_name": match[1],
                        "league_name": match[2],
                        "platform": int(platform_id),
                        "date": match[4],
                        "duration": int(match[5]),
                        "team_home": {"name": match[15],
                                      "tv": tv_home,
                                      "coach_id": coach_home_id,
                                      "conceded": match[12],
                                      "race_id": match[13],
                                      "logo": match[14],
                                      "score": match[16]
                                      },
                        "team_away": {"name": match[18],
                                      "tv": tv_away,
                                      "coach_id": coach_away_id,
                                      "conceded": match[21],
                                      "race_id": match[20],
                                      "logo": match[19],
                                      "score": match[17]
                                      },
                    }
                    match_history_db.add_or_update_match(match[0], match_data)
            else:
                spike_logger.info("No matches found in {} ".format(self.__dbFileName))

            self.close_database()
        except Exception as e:
            spike_logger.error("Error: {}".format(e))

    def write_match_data_v2(self):
        try:
            self.open_database()
            cursor = self.conn.cursor()

            cursor.execute("""SELECT uuid, competitionname, leaguename, platform, date, duration, coachhome, 
                    idcoachstatshome, tvhome, coachaway, idcoachstatsaway, tvaway, concededhome, idracehome, 
                    logohome, teamhome, scorehome, scoreaway, teamaway, logoaway, idraceaway, concededaway FROM leaguematches""")

            response = cursor.fetchall()

            if response is not None:
                spike_logger.info("Save history for: \"{}\" - \"{}\" - \"{}\"".format(response[0][2], response[0][1], response[0][3]))
                match_history_db = MatchHistory()
                coach_db = Coaches()
                rsrc_db = ResourcesRequest()
                platform_id = rsrc_db.get_platform_id(response[0][3])

                for match in response:
                    try:
                        try:
                            coach_home_id = int(match[7].split("-")[1])
                            coach_db.add_or_update_coach(coach_home_id, match[6], platform_id)
                        except:
                            coach_home_data = coach_db.get_coach_data(coach_name=match[6], platform_id=platform_id)
                            if coach_home_data is not None:
                                coach_home_id = coach_home_data["id"]
                            else:
                                raise InvalidCoach(match[6], match[0])

                        try:
                            coach_away_id = int(match[10].split("-")[1])
                            coach_db.add_or_update_coach(coach_away_id, match[9], platform_id)
                        except:
                            coach_away_data = coach_db.get_coach_data(coach_name=match[9], platform_id=platform_id)
                            if coach_away_data is not None:
                                coach_away_id = coach_away_data["id"]
                            else:
                                raise InvalidCoach(match[9], match[0])

                        spike_logger.info("  - Match: {}".format(match[0]))
                        match_data = {
                            "_id": match[0],
                            "competition_name": match[1],
                            "league_name": match[2],
                            "platform": platform_id,
                            "date": match[4],
                            "duration": int(match[5]),
                            "team_home": {"name": match[15],
                                          "tv": match[8],
                                          "coach_id": coach_home_id,
                                          "conceded": match[12],
                                          "race_id": match[13],
                                          "logo": match[14],
                                          "score": match[16]
                                          },
                            "team_away": {"name": match[18],
                                          "tv": match[11],
                                          "coach_id": coach_away_id,
                                          "conceded": match[21],
                                          "race_id": match[20],
                                          "logo": match[19],
                                          "score": match[17]
                                          },
                        }
                        match_history_db.add_or_update_match(match[0], match_data)

                    except Exception as e:
                        spike_logger.error("Error with match: {} - {}".format(match[0], e))
            else:
                spike_logger.info("No matches found in {} ".format(self.__dbFileName))

            self.close_database()
        except Exception as e:
            spike_logger.error("Error: {}".format(e))
