#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class Players(BaseMongoDb):

    def add_or_update_player(self, player, platform_id: int, team_id: int, add_starting_skill=False):
        self.open_database()
        collection = self.db["players"]
        skill_list = []
        if add_starting_skill:
            skill_list.extend(player.get_starting_skills())
        skill_list.extend(player.get_skills())
        query = {"id": player.get_id(), "platform_id": platform_id}
        new_value = {
            "$set": {
                "id": player.get_id(),
                "name": player.get_name(),
                "level": player.get_level(),
                "number": player.get_number(),
                "spp": player.get_spp(),
                "attributes": player.get_raw_attributes(),
                "skills": skill_list,
                "casualties": player.get_raw_casualties_state_id(),
                "platform_id": platform_id,
                "type": player.get_type_id(),
                "team_id": team_id
            }
        }

        if player.get_match_played():
            new_value["$set"]["match_played"] = player.get_match_played()

        if player.get_player_value():
            new_value["$set"]["value"] = player.get_player_value()

        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def add_or_update_players(self, players, platform_id: int, team_id: int, add_starting_skill=False):
        self.open_database()
        collection = self.db["players"]
        for player in players:
            skill_list = []
            if add_starting_skill:
                skill_list.extend(player.get_starting_skills())
            skill_list.extend(player.get_skills())
            query = {"id": player.get_id(), "platform_id": platform_id}
            new_value = {
                "$set": {
                    "id": player.get_id(),
                    "name": player.get_name(),
                    "level": player.get_level(),
                    "number": player.get_number(),
                    "spp": player.get_spp(),
                    "attributes": player.get_raw_attributes(),
                    "skills": skill_list,
                    "casualties": player.get_raw_casualties_state_id(),
                    "platform_id": platform_id,
                    "type": player.get_type_id(),
                    "team_id": team_id
                }
            }

            if player.get_match_played():
                new_value["$set"]["match_played"] = player.get_match_played()

            if player.get_player_value():
                new_value["$set"]["value"] = player.get_player_value()

            collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_player(self, player_id: int, platform_id: int):
        self.open_database()
        collection = self.db["players"]
        player_data = collection.find_one({"id": player_id, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return player_data
