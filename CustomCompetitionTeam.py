# -*- coding: utf-8 -*-

from bson.objectid import ObjectId

from spike_database.BaseMongoDb import BaseMongoDb


class CustomCompetitionTeam(BaseMongoDb):
    def register_team(self, team_name: str, members: list, competition_id: int, platform_id: int, team_logo=None):
        self.open_database()
        collection = self.db["custom_competition_team"]
        query = {"team_name": team_name}
        new_value = {"$set": {"team_name": team_name, "competition_id": competition_id, "platform_id": platform_id, "members": members}}
        if team_logo is not None:
            new_value["$set"]["logo"] = team_logo
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def unregister_team(self, team_name: str, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["custom_competition_team"]
        query = {"team_name": team_name, "competition_id": competition_id, "platform_id": platform_id}
        collection.delete_one(query)
        self.close_database()

    def get_teams(self, competition_id: int, platform_id: int):
        self.open_database()
        teams = []
        collection = self.db["custom_competition_team"]
        ret = collection.find({"competition_id": competition_id, "platform_id": platform_id})
        for x in ret:
            x["_id"] = str(x["_id"])
            teams.append(x)
        self.close_database()
        return teams

    def get_coach_team(self, coach_id: int, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["custom_competition_team"]
        ret = collection.find_one({"competition_id": competition_id, "platform_id": platform_id, "members.coach_id": coach_id})
        self.close_database()
        return ret

    def update_team_members(self, team_id: str, members: list):
        self.open_database()
        collection = self.db["custom_competition_team"]
        query = {"_id": ObjectId(team_id)}
        new_value = {"$set": {"members": members}}
        collection.update_one(query, new_value)
        self.close_database()
