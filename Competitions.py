#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from spike_database.BaseMongoDb import BaseMongoDb


class Competitions(BaseMongoDb):

    def add_or_update_competition(self, competition_name: str, competition_id: int, league_id: int, platform_id: int, custom_logo=None,
                                  competition_format=None):
        if competition_id is not None and competition_id > 0:
            self.open_database()
            collection = self.db["competitions"]
            query = {"id": competition_id, "platform_id": platform_id}
            competition_data = {"name": competition_name, "id": competition_id, "platform_id": platform_id, "league_id": league_id}

            if custom_logo is not None:
                competition_data["logo"] = custom_logo

            if competition_format is not None:
                competition_data["format"] = competition_format

            new_value = {"$set": competition_data}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()

    def add_or_update_raw_competition(self, competition_model, platform_id, custom_logo=None):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_model.get_id(), "platform_id": platform_id}
        competition_data = {
            "id": competition_model.get_id(),
            "name": competition_model.get_name(),
            "platform_id": platform_id,
            "format": competition_model.get_format(),
            "league_id": competition_model.get_league().get_id(),
            "date_created": competition_model.get_date_created(),
            "status": competition_model.get_status(),
            "teams_max": competition_model.get_team_max(),
            "teams_count": competition_model.get_teams_count(),
            "rounds_count": competition_model.get_rounds_count(),
            "round": competition_model.get_current_round(),
            "turn_duration": competition_model.get_turn_duration()
        }

        if custom_logo is not None:
            competition_data["logo"] = custom_logo

        new_value = {"$set": competition_data}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_competition_logo(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        competition_data = collection.find_one({"id": competition_id, "platform_id": platform_id}, {"_id": 0, "logo": 1})
        ret = None
        if competition_data is not None and "logo" in competition_data.keys():
            ret = competition_data["logo"]
        self.close_database()
        return ret

    def get_current_round(self, competition_id: int, platform_id: int, status: int = None):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id, "round": {"$exists": True}}
        if status is not None:
            query["status"] = status
        current_round = collection.find_one(query, {"_id": 0, "round": 1})
        ret = None
        if current_round is not None and "round" in current_round.keys():
            ret = current_round["round"]
        self.close_database()
        return ret

    def get_competition_data(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        competition_data = collection.find_one({"id": competition_id, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return competition_data

    def get_competitions_data(self, league_id: int, competitions_id: list, platform_id: int, fields: list = None):
        if fields is None:
            fields = []
        self.open_database()
        collection = self.db["competitions"]
        cmp_list = []
        query = {'league_id': league_id, "platform_id": platform_id, "id": {"$in": competitions_id}}
        result_filter = {"_id": 0, "name": 1, "id": 1}
        if fields is not None:
            for field in fields:
                result_filter[field] = 1
        competition_data = collection.find(query, result_filter)
        if competition_data is not None:
            for x in competition_data:
                cmp_list.append(x)
        self.close_database()
        return cmp_list

    def get_competition_data_with_name(self, competition_name: str, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        competition_data = collection.find_one({"name": competition_name, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return competition_data

    def get_point_system(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        data = collection.find_one({"id": competition_id, "platform_id": platform_id, "point_system": {"$exists": True}},
                                   {"_id": 0, "point_system": 1})
        ret = data.get("point_system") if data is not None else None
        self.close_database()
        return ret

    def set_standing(self, competition_id: int, platform_id: int, standing: list, standing_source: str, point_system: str):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        current_date = datetime.now().strftime("%Y-%m-%d %H:%M")
        new_value = {
            "$set": {"standing": standing, "standing_source": standing_source, "point_system": point_system, "standing_update": current_date}}
        collection.update_one(query, new_value)
        self.close_database()

    def get_standing(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        standing_data = collection.find_one({"id": competition_id, "platform_id": platform_id, "standing": {"$exists": True}},
                                            {"_id": 0, "standing": 1, "standing_update": 1, "standing_source": 1, "point_system": 1})
        self.close_database()
        return standing_data

    def get_standing_source(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        data = collection.find_one({"id": competition_id, "platform_id": platform_id, "standing_source": {"$exists": True}},
                                   {"_id": 0, "standing_source": 1})
        ret = data.get("standing_source") if data is not None else None
        self.close_database()
        return ret

    def is_contest_fully_collected(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        data = collection.find_one({"id": competition_id, "platform_id": platform_id, "contest_fully_collected": {"$exists": True}},
                                   {"_id": 0, "contest_fully_collected": 1})
        ret = data.get("contest_fully_collected") if data is not None else False
        self.close_database()
        return ret

    def set_contest_fully_collected(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        new_value = {"$set": {"contest_fully_collected": True}}
        collection.update_one(query, new_value)
        self.close_database()

    def set_status(self, competition_id: int, platform_id: int, status: int):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        new_value = {"$set": {"status": status}}
        collection.update_one(query, new_value)
        self.close_database()

    def set_admin_standing(self, competition_id: int, platform_id: int, admin_standing_data: dict, standing_source: str, point_system: str):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        new_value = {"$set": {
            "admin_standing": admin_standing_data,
            "standing_source": standing_source,
            "point_system": point_system
        },
            "$unset": {
                "standing": 1,
                "standing_update": 1
            }
        }

        collection.update_one(query, new_value)
        self.close_database()

    def get_admin_standing_data(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        data = collection.find_one({"id": competition_id, "platform_id": platform_id, "admin_standing": {"$exists": True}},
                                   {"_id": 0, "admin_standing": 1})
        ret = data.get("admin_standing") if data is not None else None
        self.close_database()
        return ret

    def get_competitions_in_league(self, league_id: int, platform_id: int, status: int = None):
        self.open_database()
        collection = self.db["competitions"]
        query = {"league_id": league_id, "platform_id": platform_id}
        if status is not None:
            query["status"] = status
        competitions = collection.find(query, {"_id": 0, 'standing': 0, 'top_races': 0})
        ret = list(competitions)
        self.close_database()
        return ret

    def get_filtered_competitions_like(self, competition_name: str, limit: int = 9, league_id: int = None, platform_id: int = None):
        cmp_list = []
        self.open_database()
        collection = self.db["competitions"]
        query = {"name": {'$regex': "{}".format(competition_name), "$options": "i"}, "ignore": {"$ne": 1}}
        if league_id is not None:
            query["league_id"] = league_id
        if platform_id is not None:
            query["platform_id"] = platform_id
        coaches = collection.find(query, {"_id": 0, "id": 1, "platform_id": 1, "name": 1}).limit(limit)
        for x in coaches:
            cmp_list.append({'value': x["name"], 'data': x})
        self.close_database()
        return cmp_list

    def set_top_races(self, competition_id: int, platform_id: int, top_races: dict):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        new_value = {"$set": {"top_races": top_races}}
        collection.update_one(query, new_value)
        self.close_database()

    def get_top_races(self, competition_id: int, platform_id: int):
        self.open_database()
        collection = self.db["competitions"]
        top_races = collection.find_one({"id": competition_id, "platform_id": platform_id, "top_races": {"$exists": True}},
                                        {"_id": 0, "top_races": 1})
        self.close_database()
        return top_races

    def set_custom_data(self, competition_id: int, platform_id: int, data: dict):
        self.open_database()
        collection = self.db["competitions"]
        query = {"id": competition_id, "platform_id": platform_id}
        new_value = {"$set": data}
        collection.update_one(query, new_value)
        self.close_database()
