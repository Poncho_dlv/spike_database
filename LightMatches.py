#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymongo import errors

from spike_database.BaseMongoDb import BaseMongoDb


class LightMatches(BaseMongoDb):

    def add_match(self, match_uuid, json_data):
        try:
            self.open_database()
            collection = self.db["light_matches"]
            query = {"_id": match_uuid}
            new_value = {"$set": {"match_data": json_data}}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()
        except errors.DuplicateKeyError:
            self.close_database()
            pass  # Ignore Already existing match

    def get_match_data(self, match_uuid):
        self.open_database()
        collection = self.db["light_matches"]
        query = {"_id": match_uuid}
        match_data = collection.find_one(query, {"_id": 0, "match_data": 1})
        self.close_database()
        if match_data is not None and "match_data" in match_data.keys():
            match_data = match_data["match_data"]
        return match_data

    def get_matches_in_competition(self, competition_id: int, platform: str):
        ret = []
        self.open_database()
        collection = self.db["light_matches"]
        match_data = collection.find({"match_data.idcompetition": competition_id, "match_data.platform": platform}, {"_id": 0, "match_data": 1})
        for x in match_data:
            ret.append(x["match_data"])
        self.close_database()
        return ret
