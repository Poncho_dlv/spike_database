#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class DiscordUser(BaseMongoDb):

    def add_or_update_user(self, user):
        self.open_database()
        collection = self.db["discord_user"]
        query = {"_id": user["_id"]}
        new_value = {"$set": user}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def get_user_data(self, user_id):
        self.open_database()
        collection = self.db["discord_user"]
        query = {"_id": user_id}
        user = collection.find_one(query)
        self.close_database()
        return user
