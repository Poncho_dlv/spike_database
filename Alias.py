#!/usr/bin/env python
# -*- coding: utf-8 -*-

from enum import IntEnum

from bson.errors import InvalidId
from bson.objectid import ObjectId

from spike_database.BaseMongoDb import BaseMongoDb


class AliasType(IntEnum):
    LEAGUE = 1
    COMPETITION = 2


class Alias(BaseMongoDb):

    def get_alias_data(self, alias_id: str):
        self.open_database()
        try:
            collection = self.db["alias"]
            ret = collection.find_one({'_id': ObjectId(alias_id)})
        except InvalidId:
            ret = None
        self.close_database()
        return ret

    def get_alias(self, alias_type: AliasType, platform_id: int, league_name: str = None, league_id: int = None):
        self.open_database()
        alias = []
        collection = self.db["alias"]
        query = {"type": alias_type, "platform_id": platform_id}
        if league_name is not None:
            query["league_name"] = league_name
        if league_id is not None:
            query["league_id"] = league_id
        ret = collection.find(query)
        for x in ret:
            alias.append(x)
        self.close_database()
        return alias

    def get_alias_form(self, alias_type: AliasType, platform_id: int):
        alias_list = self.get_alias(alias_type, platform_id)
        form_alias = [("", "")]
        for alias in alias_list:
            form_alias.append((str(alias.get("_id")), alias.get("name")))
        return form_alias
