#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sqlite3


class Users:
    def __init__(self):
        self.__dbFileName = "databases/users.db"

    def get_user(self, user_id: int):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT id, username, locale, discriminator, avatar FROM user WHERE id=?""", (user_id,))
        response = cursor.fetchone()
        conn.close()
        return response

    def get_user_like(self, search_username: str):
        conn = sqlite3.connect(self.__dbFileName)
        cursor = conn.cursor()
        cursor.execute("""SELECT id, username FROM user WHERE username LIKE %?%""", (search_username,))
        response = cursor.fetchall()
        conn.close()
        return response
