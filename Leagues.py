#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from typing import List

from spike_database.BaseMongoDb import BaseMongoDb


class Leagues(BaseMongoDb):

    def add_or_update_league(self, league_name: str, league_id: int, platform_id: int, league_logo=None):
        if league_id is not None and league_id > 0:
            self.open_database()
            collection = self.db["leagues"]
            query = {"id": league_id, "platform_id": platform_id}
            league_data = {"name": str(league_name), "id": int(league_id), "platform_id": int(platform_id)}

            if league_logo is not None:
                league_data["logo"] = str(league_logo)

            new_value = {"$set": league_data}
            collection.update_one(query, new_value, upsert=True)
            self.close_database()

    def add_or_update_raw_league(self, league_model, platform_id):
        self.open_database()
        collection = self.db["leagues"]
        query = {"id": league_model.get_id(), "platform_id": platform_id}
        league_data = {
            "id": league_model.get_id(),
            "name": league_model.get_name(),
            "platform_id": platform_id,
            "logo": league_model.get_logo()
        }

        if league_model.get_date_created() is not None:
            league_data["date_created"] = league_model.get_date_created()

        if league_model.is_official() is not None:
            league_data["official"] = league_model.is_official()

        if league_model.get_teams_count() is not None:
            league_data["team_count"] = league_model.get_teams_count()

        if league_model.get_last_match() is not None:
            league_data["date_last_match"] = league_model.get_last_match()

        new_value = {"$set": league_data}
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def update_competitions(self, league_id: int, platform_id: int, competitions_list: List[int]):
        if league_id is not None and league_id > 0:
            self.open_database()
            collection = self.db["leagues"]
            query = {"id": league_id, "platform_id": platform_id}
            new_value = {"$set": {"competitions": competitions_list}}
            collection.update_one(query, new_value)
            self.close_database()

    def league_exist(self, league_name: str, platform_id: int):
        self.open_database()
        collection = self.db["leagues"]
        league_data = collection.find_one({"name": league_name, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return league_data is not None

    def get_league_to_update(self, limit=30):
        self.open_database()
        leagues = []
        collection = self.db["leagues"]
        ret = collection.find({"last_update": {"$eq": None}}, {"_id": 0}).limit(limit)
        for x in ret:
            leagues.append(x)
        self.close_database()
        return leagues

    def update_league_last_update(self, league_name: str = None, platform_id: int = None, league_id: int = None):
        self.open_database()
        collection = self.db["leagues"]
        current_date = datetime.now().strftime("%Y-%m-%d")
        new_value = {"$set": {"last_update": current_date}}
        if league_name is not None and platform_id is not None:
            collection.update_many({"name": league_name, "platform_id": platform_id}, new_value)
        elif league_id is not None and platform_id is not None:
            collection.update_many({"id": league_id, "platform_id": platform_id}, new_value)
        self.close_database()

    def get_league_name_like(self, league_name: str, platform_id: int):
        ret_leagues = []
        self.open_database()
        collection = self.db["leagues"]
        query = {"name": {'$regex': ".*{}.*".format(league_name), "$options": "i"}, "platform_id": platform_id}
        leagues = collection.find(query, {"name": 1})
        for x in leagues:
            ret_leagues.append(x["name"])
        self.close_database()
        return ret_leagues

    def get_filtered_leagues_like(self, league_name: str, limit=9):
        if league_name is not None and len(league_name) > 0:
            ret_leagues = []
            self.open_database()
            collection = self.db["leagues"]
            leagues = collection.find({"name": {'$regex': "{}".format(league_name), "$options": "i"}, "ignore": {"$ne": 1}},
                                      {"_id": 0, "id": 1, "platform_id": 1, "name": 1}).limit(limit)
            for x in leagues:
                ret_leagues.append({'value': x["name"], 'data': x})
            self.close_database()
            return ret_leagues

    def get_league_logo(self, league_id: int, platform_id: int):
        self.open_database()
        collection = self.db["leagues"]
        league_data = collection.find_one({"id": league_id, "platform_id": platform_id}, {"_id": 0, "logo": 1})
        ret = None
        if league_data is not None and "logo" in league_data.keys():
            ret = league_data["logo"]
        self.close_database()
        return ret

    def get_league_data(self, league_id: int, platform_id: int):
        self.open_database()
        collection = self.db["leagues"]
        league_data = collection.find_one({"id": league_id, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return league_data

    def get_league_data_with_name(self, league_name: str, platform_id: int):
        self.open_database()
        collection = self.db["leagues"]
        league_data = collection.find_one({"name": league_name, "platform_id": platform_id}, {"_id": 0})
        self.close_database()
        return league_data

    def get_alias(self, league_id: int, platform_id: int):
        self.open_database()
        collection = self.db["leagues"]
        league_data = collection.find_one({"id": league_id, "platform_id": platform_id}, {"_id": 0, "alias": 1})
        ret = None
        if league_data is not None and league_data.get("alias"):
            ret = league_data["alias"]
        self.close_database()
        return ret

    def set_league_form_data(self, league_id: int, platform_id: int, alias=None, admins=None, discord_invite=None, website_url=None):
        self.open_database()
        collection = self.db["leagues"]
        query = {"id": league_id, "platform_id": platform_id}
        if alias is not None or admins is not None or discord_invite is not None or website_url is not None:
            new_value = {"$set": {}}
            if alias is not None:
                new_value["$set"]["alias"] = alias
            if admins is not None:
                new_value["$set"]["admins"] = admins
            if discord_invite is not None:
                new_value["$set"]["discord_invite"] = discord_invite
            if website_url is not None:
                new_value["$set"]["website_url"] = website_url
            collection.update_one(query, new_value)
        self.close_database()

    def set_custom_data(self, league_id: int, platform_id: int, data: dict):
        self.open_database()
        collection = self.db["leagues"]
        query = {"id": league_id, "platform_id": platform_id}
        new_value = {"$set": data}
        collection.update_one(query, new_value)
        self.close_database()

    def set_monitor_league(self, league_id: int, platform_id: int, monitor: bool):
        self.set_custom_data(league_id, platform_id, {"monitor": monitor})

    def get_monitored_leagues(self, platform_id: int = None):
        self.open_database()
        collection = self.db["leagues"]
        query = {"monitor": True}
        if platform_id is not None:
            query["platform_id"] = platform_id
        leagues = collection.find(query, {"id": 1, "platform_id": 1, "name": 1})
        ret = None
        if leagues is not None:
            ret = list(leagues)
        self.close_database()
        return ret
