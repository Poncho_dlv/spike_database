#!/usr/bin/env python
# -*- coding: utf-8 -*-

from spike_database.BaseMongoDb import BaseMongoDb


class TransferList(BaseMongoDb):
    def add_player(self, player_model, team_model, platform_id: int, league_id: int):
        self.open_database()
        collection = self.db["transfer_list"]

        skill_list = player_model.get_starting_skills()
        skill_list.extend(player_model.get_skills())

        query = {"id": player_model.get_id(), "platform_id": platform_id}
        attributes_modification = {
            "ma": player_model.get_ma_modification(),
            "st": player_model.get_st_modification(),
            "ag": player_model.get_ag_modification(),
            "av": player_model.get_av_modification()
        }

        attributes = {
            "ma": player_model.get_ma(),
            "st": player_model.get_st(),
            "ag": player_model.get_ag(),
            "av": player_model.get_av()
        }

        new_value = {
            "$set": {
                "id": player_model.get_id(),
                "name": player_model.get_name(),
                "level": player_model.get_level(),
                "type": player_model.get_type(),
                "race_id": team_model.get_race_id(),
                "skills": skill_list,
                "attributes": attributes,
                "attributes_modification": attributes_modification,
                "casualties": player_model.get_raw_casualties_state_id(),
                "team_name": team_model.get_name(),
                "team_id": team_model.get_id(),
                "team_logo": team_model.get_logo(),
                "coach_name": team_model.get_coach().get_name(),
                "coach_id": team_model.get_coach().get_id(),
                "spp": player_model.get_spp(),
                "value": player_model.get_player_value(),
                "league_id": league_id,
                "platform_id": platform_id
            }
        }
        collection.update_one(query, new_value, upsert=True)
        self.close_database()

    def remove_player(self, player_id: int, platform_id: int):
        self.open_database()
        collection = self.db["transfer_list"]
        query = {"id": player_id, "platform_id": platform_id}
        collection.delete_one(query)
        self.close_database()

    def get_transfer_listed(self, league_id: int, platform_id: int, race_id=None):
        self.open_database()
        players = []
        collection = self.db["transfer_list"]
        query = {"league_id": league_id, "platform_id": platform_id}
        if race_id:
            query["race_id"] = race_id
        ret = collection.find(query, {"_id": 0})
        self.close_database()
        for x in ret:
            players.append(x)
        return players
